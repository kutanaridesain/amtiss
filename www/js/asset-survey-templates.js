angular.module('amtiss.assetsurveytemplates', [])

.factory('assetSurveyTemplateModel', function($cordovaSQLite, DBA, LocalStorage) {
    var self = this;
    
    //var qsurtemplate = "CREATE TABLE IF NOT EXISTS survey_template(id integer primary key AUTOINCREMENT, server_id integer null, "
    //                +" json text null, action text null, is_delete text null )";
    
  
   
    self.flag = function() {
        var parameters  = ['Y'];
        var query       = "UPDATE survey_template set is_delete = ? ";
        return DBA.query(query, parameters);
    }
    
    self.exist = function(server_id, data) {
        var parameters = [server_id];
        return DBA.query("SELECT server_id FROM survey_template WHERE server_id = (?)", parameters)
        .then(function(result) {
            if( result.rows.length > 0){
                self.update(data);
            }else{
                self.add(data);
            }
        });
    }
    
    self.add = function( data) {
        var parameters = [ data.id, data.survey_id, data.json, data.action, 'N'];
        var query      = "INSERT INTO survey_template (server_id, survey_id, json, action, is_delete ) " +
                         "VALUES (?,?,?,?,?)";
        
        return DBA.query( query, parameters );
    }
    
    self.update = function( data ) {
       var parameters  = [ data.id, data.survey_id, data.json, data.action, 'N', data.id];
        
        var query       = "UPDATE survey_template set server_id = ?, survey_id = ?, json = ?, action = ?, is_delete = ? " +
                          "WHERE server_id = ?";
        
        return DBA.query(query, parameters);
    }
    
    self.getbysurveyid = function( survey_id ) {
        var parameters = [survey_id];
        var q = "SELECT json FROM survey_template WHERE is_delete <> 'Y'  AND survey_id = ? ";
        
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
  return self;
})