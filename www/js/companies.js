angular.module('amtiss.companies', [])

.factory('companyModel', function($cordovaSQLite, DBA) {
    var self = this;
    
    self.flag = function() {
        var parameters  = ['Y'];
        var query       = "UPDATE companies set is_delete = ? ";
        return DBA.query(query, parameters);
    }
    
    self.exist = function(server_id, data) {
        var parameters = [server_id];
        return DBA.query("SELECT server_id FROM companies WHERE server_id = (?)", parameters)
        .then(function(result) {
            if( result.rows.length > 0){
                self.update(data);
            }else{
                self.add(data);
            }
        });
    }
    
    self.add = function( data) {
        var parameters = [ data.id, data.name, data.action, 'N'];
        var query      = "INSERT INTO companies (server_id, name, action, is_delete ) " +
                         "VALUES (?,?,?,?)";
        
        return DBA.query( query, parameters );
    }
    
    self.update = function( data ) {
       var parameters  = [ data.id, data.name, data.action, 'N', data.id];
        
        var query       = "UPDATE companies set server_id = ?, name = ?, action = ?, is_delete = ? " +
                          "WHERE server_id = ?";
        
        return DBA.query(query, parameters);
    }

    self.autocomplete = function(  ) {
        var parameters = [];
        var q = "SELECT server_id as id, name as name, name as view FROM companies WHERE is_delete <> 'Y' AND action <> 'C' ";
        
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
 
    return self;
})