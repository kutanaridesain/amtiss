angular.module('amtiss.assetsurveys', [])

.factory('assetSurveyModel', function($cordovaSQLite, DBA, LocalStorage, amtissData) {
    var self = this;
    
    self.startingOfflineId = amtissData.getStartingOfflineId();
//    var qatt = "CREATE TABLE IF NOT EXISTS survey(id integer primary key AUTOINCREMENT, server_id integer null, "
//                    +" asset_id integer null, date_create text null, user_input_id integer null, "
//                    +" state text null, last_displayed integer null, survey_id integer null, "
//                    +" json text null, action text null, is_delete text null )";
    
    self.referrer = 'init';
    self.setReferrer = function( ref ){
        self.referrer = ref;
    }
    
    self.getReferrer = function(){
        return self.referrer;
    }
    
    self.flagbyassetid = function( asset_id ) {
        var parameters  = ['D', asset_id, 'O'];
        var query       = "UPDATE survey set action = ? WHERE asset_id = ? AND action = ? ";
        //console.log( parameters );
        return DBA.query(query, parameters);
    }
    
     self.updateserverid = function( server_id, user_input_id, client_id ) {
        var parameters  = [server_id, user_input_id, 'O', client_id];
         console.log( parameters )
        var query       =   "UPDATE survey set server_id = ?, user_input_id = ?, action = ? " +
                            " WHERE user_input_id = ? ";
        return DBA.query(query, parameters);
    }
    
     self.getbyassetid = function( asset_id ) {
        var parameters = [asset_id];
        //var q = "SELECT * FROM survey WHERE server_id = ? ";
        var q = "SELECT * FROM survey WHERE asset_id = ? ORDER BY date_create DESC ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
   
    self.sync = function( inspection ) {
        //console.log( inspection );
        var parameters = [inspection.server_id];
        return DBA.query("SELECT server_id FROM survey WHERE server_id = (?)", parameters)
        .then(function(result) {
            if( result.rows.length === 0 ){
                var parameters = [
                        inspection.server_id,
                        inspection.asset_id,
                        inspection.date_create,
                        inspection.user_input_id,
                        inspection.state,
                        inspection.last_displayed,
                        inspection.survey_id,
                        inspection.assignment_line_id,
                        LocalStorage.set(inspection.surveys),
                        (self.startingOfflineId < inspection.server_id)?'C':'O',
                        'N'
                ];
                
                console.log('insert');
                var query = "INSERT INTO survey (server_id, asset_id, date_create, user_input_id, "+
                            " state, last_displayed, survey_id, assignment_line_id, json, action, is_delete ) " +
                         "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                DBA.query( query, parameters );
            }else{
                console.log('update');
                var parameters = [
                        inspection.asset_id,
                        inspection.date_create,
                        inspection.user_input_id,
                        inspection.state,
                        inspection.last_displayed,
                        inspection.survey_id,
                        inspection.assignment_line_id,
                        LocalStorage.set(inspection.surveys),
                        (self.startingOfflineId < inspection.server_id)?'C':'O',
                        'N',
                        inspection.server_id,
                ];
                var query = "UPDATE survey set asset_id = ?, date_create = ?, user_input_id = ?, " +
                            " state = ?, last_displayed = ?, survey_id = ?, assignment_line_id = ?, "+
                            " json = ?, action = ?, is_delete = ? " +
                            " WHERE server_id = ? ";
                 DBA.query( query, parameters );
            }
        })
    }
                    
    self.syncOffline = function( inspection ) {
        console.log( inspection );
        var parameters = [inspection.user_input_id];
        return DBA.query("SELECT server_id FROM survey WHERE user_input_id = (?)", parameters)
        .then(function(result) {
            if( result.rows.length === 0 ){
                var offlineId = new Date().valueOf();
                var parameters = [
                        offlineId,
                        inspection.asset_id,
                        inspection.date_create,
                        //inspection.user_input_id,
                        offlineId + 1,
                        inspection.state,
                        inspection.last_displayed,
                        inspection.survey_id,
                        inspection.assignment_line_id,
                        LocalStorage.set(inspection.surveys),
                        'C',
                        'N'
                ];
                
                console.log('insert');
                var query = "INSERT INTO survey (server_id, asset_id, date_create, user_input_id, "+
                            " state, last_displayed, survey_id, assignment_line_id, json, action, is_delete ) " +
                         "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                DBA.query( query, parameters );
            }else{
                console.log('update');
                var parameters = [
                        inspection.asset_id,
                        inspection.date_create,
                        inspection.user_input_id,
                        inspection.state,
                        inspection.last_displayed,
                        inspection.survey_id,
                        inspection.assignment_line_id,
                        LocalStorage.set(inspection.surveys),
                        (self.startingOfflineId < inspection.user_input_id)?'C':'U',
                        'N',
                        inspection.user_input_id,
                ];
                console.log( parameters );
                var query = "UPDATE survey set asset_id = ?, date_create = ?, user_input_id = ?, " +
                            " state = ?, last_displayed = ?, survey_id = ?, assignment_line_id = ?, "+
                            " json = ?, action = ?, is_delete = ? " +
                            " WHERE user_input_id = ? ";
                 DBA.query( query, parameters );
            }
        })
    }
                    
    self.getbyserverid = function( server_id ) {
        var parameters = [server_id];
        //var q = "SELECT * FROM survey WHERE server_id = ? ";
        var q = "SELECT * FROM survey WHERE server_id = ? ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getById(result);
        });
    }
            
    self.getbyuserinputid = function( user_input_id ) {
        var parameters = [user_input_id];
        //var q = "SELECT * FROM survey WHERE server_id = ? ";
        var q = "SELECT * FROM survey WHERE user_input_id = ? ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getById(result);
        });
    }
            
    self.removebyflag = function() {
        var parameters = ['D'];
        return DBA.query("DELETE FROM survey WHERE action = ?", parameters);
    }
    
  return self;
})