angular.module('amtiss.assetimages', [])

.factory('assetImagesModel', function($cordovaSQLite, DBA, $q) {
    var self = this;
    
    self.flagbyassetid = function( asset_id ) {
        var parameters  = ['D', asset_id, 'O'];
        var query       = "UPDATE asset_images set action = ? WHERE asset_id = ? AND action = ? ";
        return DBA.query(query, parameters);
    }
    
    self.async = function( picture, uid, asset_id ) {
        var parameters = [picture.attachment_id];
        return DBA.query("SELECT server_id FROM asset_images WHERE server_id = (?)", parameters)
        .then(function(result) {
            if( result.rows.length === 0 ){
                //insert
                if( picture.attachment_data !== null ){
                    var param = {
                        server_id: picture.attachment_id,
                        uid: uid,
                        asset_id: asset_id,
                        name: picture.attachment_file_name,
                        filesize: picture.attachment_file_size,
                        res_name: picture.attachment_asset_name,
                        file_type: picture.attachment_file_type ,
                        base64: 'data:'+picture.attachment_file_type+';base64,'+picture.attachment_data,
                        action: null
                    };
                    
                    //console.log( param );
                    self.add( param, 'O' );
                }
                
            }else{
                self.unflagbyserverid( picture.attachment_id );
            }
        });
    }
    
    self.add = function( data, action ) {
        data.action = action;
        var parameters = [ data.server_id, data.uid, data.asset_id, data.name, data.filesize, data.res_name, data.file_type, data.base64, data.action, 'N'];

        var query      = "INSERT INTO asset_images (server_id, uid, asset_id, name, filesize, res_name, file_type, base64, action, is_delete ) " +
                         "VALUES (?,?,?,?,?,?,?,?,?,?)";
        //console.log( 'add : ' + data.id );
        return DBA.query( query, parameters );
    }
    
    self.unflagbyserverid = function( server_id ){
        var parameters  = ['O', server_id];
        //console.log( parameters );
        var query       = "UPDATE asset_images set action= ?  WHERE server_id = ? ";
        return DBA.query(query, parameters);
    }
    
    self.removebyflag = function() {
        return DBA.query("DELETE FROM asset_images WHERE action = 'D'", null);
    }
    
    self.getbyassetid = function( asset_id ) {
        //console.log( asset_id );
        var parameters = [asset_id];
        var q = "SELECT * FROM asset_images WHERE asset_id = ? " +
                " AND action <> 'D' ";
        //console.log( q );
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
     self.getallbyassetid = function( asset_id ) {
        //console.log( asset_id );
        var parameters = [asset_id];
        var q = "SELECT * FROM asset_images WHERE asset_id = ? ";
        //console.log( q );
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
    self.getpicturebyassetid = function( asset_id, index ) {
        //console.log( asset_id );
        var parameters = [asset_id];
        var q = "SELECT base64 FROM asset_images WHERE asset_id = ? " +
                " AND action <> 'D' ORDER BY server_id DESC LIMIT 1 ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return {'picture':DBA.getAll(result), 'index':index };
        });
    }
    
    
    self.getidbyassetid = function( asset_id ) {
        //console.log( asset_id );
        var parameters = [asset_id];
        var q = "SELECT id FROM asset_images WHERE asset_id = ? " +
                " AND action <> 'D' ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
    self.updateserverid = function(server_id, client_id) {
        var parameters  = [server_id, 'O', client_id];
        //console.log( parameters );
        var query       = "UPDATE asset_images set server_id = ?, action= ?  WHERE id = ? ";
        return DBA.query(query, parameters);
    }
    
     self.updateserveridandassetid = function(server_id, client_id, asset_id) {
        var parameters  = [server_id, asset_id, 'O', client_id];
        //console.log( parameters );
        var query       = "UPDATE asset_images set server_id = ?, asset_id = ?, action= ?  WHERE id = ? ";
        return DBA.query(query, parameters);
    }
    
    self.getbyassetwithindex = function( asset_id , i) {
        var parameters = [asset_id];
        return DBA.query("SELECT base64 FROM asset_images WHERE asset_id = (?) AND is_delete = 'N' " +
                         " AND action <> 'D' ORDER BY server_id DESC LIMIT 1 OFFSET 0 ", parameters)
        .then(function(result) {
            var picture = DBA.getAll(result);
            return {picture:picture, index: i};
        });
    }
    
     self.byasset = function( asset_id ) {
        var parameters = [ asset_id ];
        var q = "SELECT * FROM asset_images WHERE asset_id = ? AND action <> 'D' ORDER BY server_id DESC ";
        
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
     
    self.flagbyid = function( id ) {
        var parameters  = ['D', id];
        var query       = "UPDATE asset_images set action = ? WHERE id = ? ";
        return DBA.query(query, parameters);
    }
    
    self.getflagged = function( ) {
        var parameters = ['D'];
        var q = "SELECT * FROM asset_images WHERE action = ? ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
     
    self.removebyid = function(id) {
        var parameters = [id];
        return DBA.query("DELETE FROM asset_images WHERE id = (?)", parameters);
    }
    
    self.removebyserverid = function(server_id) {
        var parameters = [server_id];
        return DBA.query("DELETE FROM asset_images WHERE server_id = (?)", parameters);
    }

  return self;
})