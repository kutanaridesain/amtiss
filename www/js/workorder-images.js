angular.module('amtiss.workorderimages', [])

.factory('workorderImagesModel', function($cordovaSQLite, DBA, $q) {
    var self = this;
    
    self.flagbyuid = function(uid) {
        var parameters  = ['D', uid, 'O'];
        var query       = "UPDATE workorder_images set action = ? WHERE uid = ? AND action = ? ";
        return DBA.query(query, parameters);
    }
    
//    self.flagbywolineid = function( wo_line_id ) {
//        var parameters  = ['D', wo_line_id, 'O'];
//        var query       = "UPDATE workorder_images set action = ? WHERE wo_line_id = ? AND action = ? ";
//        return DBA.query(query, parameters);
//    }
    
    self.async = function( picture, uid ) {
        var parameters = [picture.server_id];
        return DBA.query("SELECT server_id FROM workorder_images WHERE server_id = (?)", parameters)
        .then(function(result) {
            if( result.rows.length === 0 ){
                //insert
                if( picture.base64 !== null ){
                    var param = {
                        server_id: picture.server_id,
                        uid: uid,
                        wo_id: picture.wo_id,
                        wo_line_id: picture.wo_line_id,
                        asset_id: picture.asset_id,
                        name: picture.name,
                        filesize: picture.file_size,
                        res_name: picture.res_name,
                        file_type: picture.file_type ,
                        base64: 'data:'+picture.file_type+';base64,'+picture.base64,
                        action: null
                    };
                    
                    console.log( 'async' );
                    self.add( param, 'O' );
                }
                
            }else{
                //console.log( 'unflagbyserver id');
                self.unflagbyserverid( picture.server_id );
            }
        });
    }
    
    self.add = function( data, action ) {
        data.action = action;
        var parameters = [ data.server_id, data.uid, data.wo_id, data.wo_line_id, data.asset_id, data.name, data.filesize, 
                          data.res_name, data.file_type, data.base64, data.action, 'N'];

        var query      = "INSERT INTO workorder_images (server_id, uid, wo_id, wo_line_id, asset_id, name, filesize, res_name, file_type, base64, action, is_delete ) " +
                         "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        //console.log( 'add : ' + data.id );
        return DBA.query( query, parameters );
    }
    
    self.unflagbyserverid = function( server_id ){
        var parameters  = ['O', server_id];
        //console.log( parameters );
        var query       = "UPDATE workorder_images set action= ?  WHERE server_id = ? ";
        return DBA.query(query, parameters);
    }
    
    self.removebyflag = function() {
        return DBA.query("DELETE FROM workorder_images WHERE action = 'D'", null);
    }
    
    self.getbywolineid = function( wo_line_id ) {
        //console.log( wo_line_id );
        var parameters = [wo_line_id];
        var q = "SELECT * FROM workorder_images WHERE wo_line_id = ? " +
                " AND action <> 'D' ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
    self.getidbywolineid = function( wo_line_id ) {
        //console.log( wo_line_id );
        var parameters = [wo_line_id];
        var q = "SELECT id FROM workorder_images WHERE wo_line_id = ? " +
                " AND action <> 'D' ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
    self.updateserverid = function(server_id, client_id) {
        var parameters  = [server_id, 'O', client_id];
        //console.log( parameters );
        var query       = "UPDATE workorder_images set server_id = ?, action= ?  WHERE server_id = ?";
        return DBA.query(query, parameters);
    }
    
    self.getbyassetwithindex = function( wo_line_id , i) {
        var parameters = [wo_line_id];
        return DBA.query("SELECT base64 FROM workorder_images WHERE wo_line_id = (?) AND is_delete = 'N' " +
                         " AND action <> 'D' ORDER BY id LIMIT 1 OFFSET 0 ", parameters)
        .then(function(result) {
            var picture = DBA.getAll(result);
            return {picture:picture, index: i};
        });
    }
    
     self.bywoline = function( wo_line_id ) {
        var parameters = [ wo_line_id ];
        var q = "SELECT * FROM workorder_images WHERE wo_line_id = ? AND is_delete = 'N' ORDER BY id ";
        
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
     
    self.flagbyserverid = function( server_id ) {
        var parameters  = ['D', server_id];
        var query       = "UPDATE workorder_images set action = ? WHERE server_id = ? ";
        return DBA.query(query, parameters);
    }
    
    self.getflagged = function( ) {
        var parameters = ['D'];
        var q = "SELECT * FROM workorder_images WHERE action = ? ";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
     
    self.removebyid = function(id) {
        var parameters = [id];
        return DBA.query("DELETE FROM workorder_images WHERE id = (?)", parameters);
    }
    
    self.removebyserverid = function(server_id) {
        var parameters = [server_id];
        return DBA.query("DELETE FROM workorder_images WHERE server_id = (?)", parameters);
    }

  return self;
})