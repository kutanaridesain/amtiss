angular.module('amtiss.assets', [])

.factory('assetModel', function ( $cordovaSQLite ) {
    
    return({
        selectByUserId : selectByUserId,
        selectById : selectById,
        setAsDelete : setAsDelete,
        isNew : isNew,
        insert: insert,
        update: update,
    });
    
    function selectByUserId( uid ){
         var q = "SELECT * FROM assets WHERE uid = ?";
         var where = [uid];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            return result;
        },
        function(error) {
            console.error('Asset ' + error.message);
            return -1;
        });
    }
    
    function selectById( server_id ){
         var q = "SELECT * FROM assets WHERE server_id = ?";
         var where = [server_id];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            return result;
        },
        function(error) {
            console.error('Asset ' + error.message);
            return -1;
        });
    }
    
    function setAsDelete( uid ){
        var q = "UPDATE assets set is_delete = ? WHERE uid = ? ";
            
        var params = [ 'Y', uid ];
            
        $cordovaSQLite.execute(db, q, params).then(function(result) {
            return 1;
        }, function(error) {
            console.error('Asset ' + error.message);
            return -1;
        });
    }
    
    function isNew( server_id ){
         var q = "SELECT * FROM assets WHERE server_id = ?";
         var where = [server_id];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            //console.log('Length on is New : ' + result.rows.length);
            if(result.rows.length === 0) {
                return 1;   //ada
            }else{
                return 0;   //tidak ada
            }
        },
        function(error) {
            console.error('Asset ' + error.message);
            return -1;      //error
        });
    }
    
    function insert( data ){
//       q = "CREATE TABLE IF NOT EXISTS assets (id integer primary key AUTOINCREMENT, server_id int, uid int, code_reference text, "
//        +" asset_name text null, location_id int null, location_name text null, team_id int null, team_name text null, "
//                    +" category_id int null, category_name text null, latitude text null, longitude text null, "
//                    +" barcode text null, description text null, is_delete text null "
//                    +" )"; 
        var q = "INSERT INTO assets (server_id, uid, code_reference, asset_name, location_id, location_name, team_id, team_name, " +
                "category_id, category_name, latitude, longitude, barcode, description, is_delete ) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
        var params = [ data.id, data.uid, data.code_reference, data.asset_name, data.location_id, data.location_name,
                        data.team_id, data.team_name, data.category_id, data.category_name, data.latitude, data.longitude, 
                        data.barcode, data.description, 'N'];
            
        $cordovaSQLite.execute(db, q, params).then(function(result) {
            return result.insertId;
        }, function(error) {
            console.error('Asset ' + error.message);
            return -1;
        });
    }
    
    function update( data ){
        var q = "UPDATE assets set server_id = ?, uid = ?, code_reference = ?, asset_name = ?, location_id = ?, " +
                "location_name = ?, team_id = ?, team_name = ?, category_id = ?, category_name = ?, "
                "latitude = ?, longitude = ?, barcode = ?, descrption = ?, is_delete = ? " +
                "WHERE server_id = ? ";
            
        var params = [ data.id, data.uid, data.code_reference, data.asset_name, data.location_id, data.location_name,
                        data.team_id, data.team_name, data.category_id, data.category_name, data.latitude, data.longitude, 
                        data.barcode, data.description, 'N', data.id];
            
        $cordovaSQLite.execute(db, q, params).then(function(result) {
            return data.id;
        }, function(error) {
            console.error('Asset ' + error.message);
            return -1;
        });
    }
    
});