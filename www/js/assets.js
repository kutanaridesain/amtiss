angular.module('amtiss.assets', [])

.factory('assetModel', function($cordovaSQLite, $q, DBA) {
    var self = this;
    
    self.flag = function( uid ) {
        var parameters  = ['Y', 'O', uid];
        //console.log( parameters );
        var query       = "UPDATE assets set is_delete = ? "+
                          "WHERE action = ? AND uid = ? ";
        
        return DBA.query(query, parameters);
    }
    
    self.addOrUpdateAsset = function( assetparameters ){
        //console.log( assetparameters );
        var q = $q.defer();
        self.exist( assetparameters.server_id ).then(
            function( result ){
                //console.log( 'exist' );
                if( result > 0){
                    //update 
                    q.resolve( self.update ( assetparameters ) );
                }else{
                    //insert
                    q.resolve( self.add ( assetparameters ) );
                }
            }
        );
        
        return q.promise;
    };
    
    self.exist = function(server_id) {
        var parameters = [server_id];
        return DBA.query("SELECT server_id FROM assets WHERE server_id = (?)", parameters)
        .then(function(result) {
            return result.rows.length;
        });
    }
    
    self.add = function( data) {
        var parameters = [ data.server_id, data.uid, data.code_reference, data.asset_name, data.location_id, data.location_name,
                           data.team_id, data.team_name, data.category_id, data.category_name, data.parent_id, data.parent_name, 
                           data.company_id, data.company_name, data.latitude, data.longitude, 
                           data.barcode, data.description, data.address, data.create_date, data.survey_id, data.action, 'N'];
        
        var query      = "INSERT INTO assets (server_id, uid, code_reference, asset_name, location_id, location_name, team_id, team_name, " +
                         "category_id, category_name, parent_id, parent_name, company_id, company_name, latitude, longitude, barcode, " +
                         "description, address, create_date, survey_id, action, is_delete ) " +
                         "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        //console.log( 'add');
        return DBA.query( query, parameters );
    }
    
    self.update = function( data ) {
         var parameters  = [ data.server_id, data.uid, data.code_reference, data.asset_name, data.location_id, data.location_name,
                            data.team_id, data.team_name, data.category_id, data.category_name, data.parent_id, data.parent_name, 
                            data.company_id, data.company_name, data.latitude, data.longitude, 
                            data.barcode, data.description, data.address, data.create_date, data.survey_id, data.action, 'N', data.server_id];
        
        var query       = "UPDATE assets set server_id = ?, uid = ?, code_reference = ?, asset_name = ?, location_id = ?, " +
                          "location_name = ?, team_id = ?, team_name = ?, category_id = ?, category_name = ?, " +
                          "parent_id = ?, parent_name = ?, company_id = ?, company_name = ?, latitude = ?, longitude = ?, " + 
                          "barcode = ?, description = ?, address = ?, create_date = ?, survey_id = ?, action = ?, is_delete = ? " +
                          "WHERE server_id = ? ";
        //Untuk yang sinkronisasi, maka hanya yang action 'O' yang di update 
        if( data.sync === true ){
             parameters  = [ data.server_id, data.uid, data.code_reference, data.asset_name, data.location_id, data.location_name,
                            data.team_id, data.team_name, data.category_id, data.category_name, data.parent_id, data.parent_name, 
                            data.company_id, data.company_name, data.latitude, data.longitude, data.barcode, data.description, 
                            data.address, data.create_date, data.survey_id, data.action, 'N', data.server_id, 'O'];
        
            query       = "UPDATE assets set server_id = ?, uid = ?, code_reference = ?, asset_name = ?, location_id = ?, " +
                          "location_name = ?, team_id = ?, team_name = ?, category_id = ?, category_name = ?, " +
                          "parent_id = ?, parent_name = ?, company_id = ?, company_name = ?, latitude = ?, longitude = ?, " + 
                          "barcode = ?, description = ?, address = ?, create_date = ?, survey_id = ?, action = ?, is_delete = ? " +
                          "WHERE server_id = ? AND action = ?";
        }
      
        
        //console.log( 'update' );
        return DBA.query(query, parameters);
    }
    
    self.removebyflag = function(uid) {
        var parameters = ['Y', uid];
        return DBA.query("DELETE FROM assets WHERE is_delete = ? AND uid = (?)", parameters);
    }

    self.getbyuser = function( user_id ) {
        var parameters = [user_id];
        var q = "SELECT a.* " + 
                " '[' || " +
                "  GROUP_CONCAT( " +
                "        '{'id':' || " +
                "          b.id || " +
                "          ', \"asset_attributes_name\":\"' || " +
                "          b. `asset_attributes_name` || " + 
                "          '\", \"attribute_value_name\":\"' || " +
                "          b.attribute_value_name || " +
                "          '\"}' " +
                "  ) || ']' "+
                "  AS people " +
                " FROM assets a " +
                " LEFT JOIN attributes AS b ON a.server_id = b.assest_id " +
                " GROUP BY a.server_id, a.code_reference " +
                " WHERE a.uid = (?) ORDER BY a.asset_name";
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
    
    self.getbyid = function(server_id, uid) {
        var parameters = [Number(server_id), uid];
        //console.log( parameters ); 
        return DBA.query("SELECT *, '' as src FROM assets WHERE server_id = (?) AND uid = (?)", parameters)
        .then(function(result) {
            return DBA.getById( result );
        });
    }
    
    self.getserverids = function( uid ){
        var parameters = [uid];
        var q = "SELECT server_id FROM assets WHERE uid = (?) ORDER BY server_id " 

        return DBA.query( q, parameters)
        .then(function(result) {
            //return DBA.getAll(result);
            var output = [];

            for (var i = 0; i < result.rows.length; i++) {
                output.push(result.rows.item(i).server_id);
            }
        
            return output;
        });
    }
    
    self.prev = function(server_id, uid) {
        var parameters = [server_id, uid];
        return DBA.query("SELECT server_id FROM assets WHERE (server_id < (?)) AND (uid = (?)) ORDER BY server_id LIMIT 1 OFFSET 0", parameters)
        .then(function(result) {
            if(result.rows.length > 0){
                return result.rows.item(0).server_id;
            }else{
                return 0;
            }
        });
    }
    
    self.next = function(server_id, uid) {
        var parameters = [server_id, uid];
        return DBA.query("SELECT server_id FROM assets WHERE (server_id > (?)) AND (uid = (?)) ORDER BY server_id LIMIT 1 OFFSET 0", parameters)
        .then(function(result) {
            if(result.rows.length > 0){
                
                return result.rows.item(0).server_id;
            }else{
              
                return 0;
            }
        });
    }
    
    self.autocomplete = function( uid ) {
        var parameters = [uid];
        var q = "SELECT server_id as id, ( code_reference || ' - ' || asset_name ) as name, ( code_reference || ' - ' || asset_name ) as view " +
                "FROM assets WHERE is_delete <> 'Y' AND uid = ? ";
        
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
     
    self.getbybarcode = function(barcode, uid) {
        var parameters = [barcode, uid];
        return DBA.query("SELECT server_id FROM assets WHERE barcode = (?) AND uid = ?", parameters)
        .then(function(result) {
            if( result.rows.length > 0 ){
                return result.rows.item(0).server_id;
            }else{
                return 0;
            }
        });
    }
    
    self.serveroflineid = function( $startlocalid ) {
        var parameters = [$startlocalid];
        return DBA.query("SELECT server_id FROM assets WHERE (server_id > (?)) ORDER BY server_id DESC LIMIT 1 OFFSET 0", parameters)
        .then(function(result) {
            if(result.rows.length > 0){
                
                return result.rows.item(0).server_id;
            }else{
              
                return $startlocalid;
            }
        });
    }
    
    self.getuniqueassets = function(uid) {
        return DBA.query("SELECT distinct(server_id) FROM assets WHERE uid=" + uid )
        .then(function(result){
            return DBA.getAll(result);
        });
    }
    
    self.updateOfflineId = function( online_id, offline_id ) {
        var parameters  = [ online_id, 'O', offline_id ];
        var query       = "UPDATE assets set server_id = ?, action = ? "+
                          "WHERE server_id = ? ";
        return DBA.query(query, parameters);
    }

    return self;
})