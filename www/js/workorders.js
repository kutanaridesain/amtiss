angular.module('amtiss.workorders', [])

.factory('workOrderModel', function($cordovaSQLite, $q, DBA, LocalStorage) {
    var self = this;
    
    self.sync = function( workorders, callback ){
        //console.log( 'flag' )
        var parameters  = ['Y', 'O'];
        var query       = "UPDATE workorders set is_delete = ? "+
                          "WHERE action = ? ";
        DBA.query(query, parameters);
        
        var defer = $q.defer();
        var promises = [];
        angular.forEach(workorders, function(value, key) { 
            promises.push( self.exist( value.id, value) );
        });

        $q.all(promises).then(function() {
            self.removebyflag();
            //console.log('delete');
            callback();
        });
    }
      
    self.exist = function(server_id, data) {
        var parameters = [server_id];
        return DBA.query("SELECT server_id FROM workorders WHERE server_id = (?)", parameters)
        .then(function(result) {
            
           
            if( result.rows.length > 0 ){
                //edit
                 var parameters = [ data.id, data.uid, data.workorder_name, data.assign_date, data.location_id, data.location_name,
                           data.team_id, data.team_name, data.category_id, data.category_name, data.assign_group,
                           data.create_date, data.state,  LocalStorage.set(data.json), 'O', 'N', data.id];
                var query       = "UPDATE workorders set server_id = ?, uid = ?, name = ?, assign_date = ?, location_id = ?, " +
                                  "location_name = ?, team_id = ?, team_name = ?, category_id = ?, category_name = ?, " +
                                  "assign_group = ?, create_date = ?, state = ?, json = ?, action = ?, is_delete = ? " +
                                  "WHERE server_id = ? ";

                //console.log( 'update' );
                return DBA.query(query, parameters);
            }else{
                //insert
                 var parameters = [ data.id, data.uid, data.workorder_name, data.assign_date, data.location_id, data.location_name,
                           data.team_id, data.team_name, data.category_id, data.category_name, data.assign_group,
                           data.create_date, data.state,  LocalStorage.set(data.json), 'O', 'N'];
                var query      = "INSERT INTO workorders (server_id, uid, name, assign_date, location_id, location_name, team_id, team_name, " +
                                 "category_id, category_name, assign_group, create_date, state, json, action, is_delete ) " +
                                 "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                //console.log( 'add');
                return DBA.query( query, parameters );
            }
        });
    }
    
    self.removebyflag = function() {
        var parameters = ['Y'];
        return DBA.query("DELETE FROM workorders WHERE is_delete = ? ", parameters);
    }
     
    self.getbyid = function(server_id) {
        var parameters = [Number(server_id)];
        return DBA.query("SELECT * FROM workorders WHERE server_id = (?) ", parameters)
        .then(function(result) {
            return DBA.getById( result );
        });
    }
    
    self.updatejsonbyid = function(state, json, id ) {
        var parameters = [state, json, id];
        return DBA.query("UPDATE workorders set state = ?, json= ? WHERE server_id = ? ", parameters);
    }
    
     self.getbyuserid = function(userid) {
         var parameters = [userid];
        return DBA.query("SELECT json FROM workorders where uid = ? ", parameters)
        .then(function(result){
            return DBA.getAll(result);
        });
    }
     
    return self;
})