angular.module('amtiss.services', ['amtiss.users', 'amtiss.assets', 'amtiss.attributes', 'amtiss.locations',
                                  'amtiss.categories', 'amtiss.companies', 'amtiss.vendors', 'amtiss.teams',
                                  'amtiss.atts', 'amtiss.assetimages', 'amtiss.assetsurveys',
                                  'amtiss.assetsurveytemplates', 'amtiss.workorders', 
                                  'amtiss.workorderimages'])

.filter('trustHtml', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml( text );
        };
}])

.factory('amtissService', function ($http, $q, $ionicLoading, $timeout, amtissData) {
    var self = this;
    self.timeout = 10;
    return({
        doRequest: doRequest
    });
    
    function doRequest( act, params, hideIndicator, timeout ){
//        if( typeof loadingText === 'undefined' || loadingText === null ){
//            loadingText = 'Loading...';
//        }
//        
        if( typeof timeout !== 'undefined' ){
            self.timeout = timeout;
        }
        if(  typeof hideIndicator === 'undefined' || hideIndicator === null ){
            $ionicLoading.show({
              //template: loadingText
            });
            //console.log( 'show indicator' );
        }
    
        var request = $http({
            method: "post",
            url: amtissData.baseURL+act,
            data:params
        });

        return( request.then( handleSuccess, handleError ) );
    }
    
    function handleError( response ) {
        
        $ionicLoading.hide();
        
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
        ) {
            return( $q.reject( "AMTISS error." ) );
        }
        return( $q.reject( response.data.message ) );
    }
    
    function handleSuccess( response ) {
        $timeout(function(){
            $ionicLoading.hide();
        }, self.timeout);
        
        return( response.data );
    }
})

/* data */
.factory('amtissData', function(){
    return {
        appTitle: 'AMTISS',
        getAppTitle: function(){
            return this.appTitle;
        },
        baseURL: 'http://103.43.44.216:8099/amtiss/index.php/',
        getBaseURL: function() {
            return this.baseURL;
        },
        token:'',
        getToken: function(){
            return this.token;
        },
        setToken: function( value ){
            this.token = value;
        },
        uid:'',
        getUid: function(){
            //TODO: REMOVE HARDCODE
            return this.uid===''?1:this.uid;
        },
        setUid: function( value ){
            this.uid = value;
        },
        username:'',
        getUsername: function(){
            return this.username;
        },
        setUsername: function( value ){
            this.username = value;
        },
        company:'',
        getCompany: function(){
            return this.company;
        },
        setCompany: function( value ){
            this.company = value;
        },
        messages: {
            'no_internet_connection':'Please check your internet connection or try again later.',
            'no_asset_found':'No asset found.',
            'logout_confirmation':'Are you sure you want to log out?',
            'close_app_confirmation': 'Are you sure you want to quit?',
            'database_error':'Database error.',
            'not_valid_user':'Username or password is not valid.',
            'no_wo_found':'No workorder found.',
            'offline_mode':'The data will be stored offline in your device. Please synchronized with servers once you’re back online.',
        },
        getMessage: function( key ){
            return this.messages[key];
        },
        getStartingOfflineId: function(){
            return 10000000;
        }
    };
})

.factory('LocalStorage', [function() {
        return {
            set: function(value) {
                return JSON.stringify(value);
            },
            get: function(value) {
                return JSON.parse( value );
            }
        };
    }]
)

/* ink */
.directive('ngLastRepeat', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngLastRepeat'+ (attr.ngLastRepeat ? '.'+attr.ngLastRepeat : ''));
                }, 100);
//                console.log( new Date().toString() );
//                console.log( element );
//                console.log( attr );
            }
        }
    };
});