angular.module('amtiss.users', [])

.factory('DBA', function($cordovaSQLite, $q, $ionicPlatform) {
    var self = this;

    // Handle query's and potential errors
    self.query = function (query, parameters) {
        parameters = parameters || [];
        var q = $q.defer();

        $ionicPlatform.ready(function () {
            $cordovaSQLite.execute(db, query, parameters)
            .then(function (result) {
                q.resolve(result);
            }, function (error) {
                console.warn('I found an error');
                console.warn(error.message);
                console.log(query);
                q.reject(error);
            });
        });
        
        return q.promise;
    }

    // Proces a result set
    self.getAll = function(result) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }
        
        return output;
    }

    // Proces a single result
    self.getById = function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
    }
    
    self.escape = function( str ) {
        return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    }

    return self;
})

.factory('userModel', function($cordovaSQLite, DBA) {
    var self = this;
    
    self.exist = function(server_id) {
        var parameters = [server_id];
        return DBA.query("SELECT server_id FROM user WHERE server_id = (?)", parameters)
        .then(function(result) {
            return result.rows.length;
        });
    }
    
    self.add = function( user) {
        var parameters  =   [ user.uid, user.fullname, user.username, user.password, user.company_name, user.picture, 'N' ];
        var query       =   "INSERT INTO user (server_id, full_name, username, password, company_name, picture, is_delete ) " +
                            "VALUES ( ?, ?, ?, ?, ?, ?, ? )";
        
        return DBA.query( query, parameters );
    }
    
    self.update = function( user ) {
       // var parameters = [editMember.id, editMember.name, origMember.id];
        var parameters = [ user.uid, user.fullname, user.username, user.password, user.company_name,
                           user.picture, 'N', user.uid];
        var query   = "UPDATE user set server_id = ?, full_name = ?, username = ?, password = ?, " +
                      "company_name = ?, picture = ?, is_delete = ? WHERE server_id = ? ";
        
         
        return DBA.query(query, parameters);
    }
    
    self.getbyaccount = function(username, password) {
        var parameters = [username, password];
        return DBA.query("SELECT * FROM user WHERE username = ? AND password = ?", parameters)
          .then(function(result) {

            return DBA.getById(result);
          });
      }
    
    self.test = function() {
        return DBA.query("SELECT * FROM user")
        .then(function(result){
            for(var i=0;i<result.rows.length;i++){
                console.log('Fullname: ' + result.rows.item(i).full_name);
                console.log('Username: ' + result.rows.item(i).username);
            }
            //return DBA.getAll(result);
        });
    }
    
    return self;
})