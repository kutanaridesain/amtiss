angular.module('amtiss.controllers', [])

.controller('LoginCtrl', function($scope, $ionicPopup,
                                    $state, $timeout,
                                    $cordovaSQLite, 
                                    $ionicPlatform,
                                    $base64,
                                    amtissService,
                                    amtissData, 
                                    assetModel, 
                                    userModel,
                                    attributeModel
                                  ) {
    $scope.data = [];
    
    $scope.dropTable = function (){
            var q = "DROP TABLE user";
            $cordovaSQLite.execute(db, q);
        
            var q = "DROP TABLE assets";
            $cordovaSQLite.execute(db, q);
        
            var q = "DROP TABLE attributes";
            $cordovaSQLite.execute(db, q);
        
            var q = "DROP TABLE employee";
            $cordovaSQLite.execute(db, q);
        
            console.log('Done....');
    }
    
    $scope.init = function() {
        $scope.data.username = 'admin';
        $scope.data.password = 'in0t3ch';
        //$scope.data.username = '';
        //$scope.data.password = '';
        
        //alert(md5.createHash('hello'));
        //userModel.test();
    }
    
    $scope.validateLogin = function( isValid ){
        if( isValid ){
            $ionicPlatform.ready(function() {
                if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
                    if(window.Connection) {
                        if(navigator.connection.type == Connection.NONE) {
//                            var alertPopUp = $ionicPopup.alert({
//                                title: amtissData.getAppTitle(),
//                                template: amtissData.getMessage( 'no_internet_connection' )
//                            });
                            $scope.loginlocal();
                        }else{
                            $scope.login();
                        }
                    }else{
//                        var alertPopUp = $ionicPopup.alert({
//                            title: amtissData.getAppTitle(),
//                            template: amtissData.getMessage( 'no_internet_connection' )
//                        });
                        $scope.loginlocal();
                    }
                }else{
                    if( navigator.onLine === false ) {
//                        var alertPopUp = $ionicPopup.alert({
//                            title: amtissData.getAppTitle(),
//                            template: amtissData.getMessage( 'no_internet_connection' )
//                        });
                        $scope.loginlocal();
                    }else{
                        $scope.login();
                    }
                } 
            });
        }
    }
    
    $scope.loginlocal = function(){
        var username64 = $base64.encode($scope.data.username);
        var password64 = $base64.encode($scope.data.password);
        
        userModel.getbyaccount( username64, password64 ).then(
            function( result ){
                if( result != null ){
                    //TODO REMOVE HARCODE
                    amtissData.setToken( 'offline token' );
                    amtissData.setUid( result.server_id );
                    $state.go('app.assets')
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: amtissData.getMessage('not_valid_user')
                    });
                    
                }
            }
        );
        
            
//            var q = "SELECT name FROM sqlite_master ";
//            $cordovaSQLite.execute(db, q, []).then(function(p) {
//                
//                console.log('................................');
//                for(var i=0;i<p.rows.length;i++){
//                    var x = p.rows.item(i);
//                    for (var key in x) {
//                      if (x.hasOwnProperty(key)) {
//                        console.log(key + " -> " + x[key]);
//                      }
//                    }
//                }
//            
//            
//            }, function(p) {
//                //console.error(error.message);
//                for (var key in p) {
//                  if (p.hasOwnProperty(key)) {
//                    console.log(key + " -> " + p[key]);
//                  }
//                }
//            });
    }
    
    $scope.login = function() {
    
        $scope.param = { isajax:1, username:$scope.data.username, password:$scope.data.password, key: 'amtiss' };

        amtissService.doRequest( 'login', $scope.param )
        .then(
            function( response ) { 
                if( response.status === true ){

                    if( typeof response.data !== 'undefined'){            
                        if( typeof response.data.assets !== 'undefined'){
                            //set token and uid
                            amtissData.setToken( response.token );
                            amtissData.setUid( response.uid );
                                    
                            /* USER */
                            var usernameMD5 = $base64.encode($scope.data.username);
                            var passwordMD5 = $base64.encode($scope.data.password);
                                    
                            var userparameters = { uid:response.uid, fullname:'FULLNAME Add' , username:usernameMD5, 
                                                   password:passwordMD5, company_name:null, picture:null };
                                    
                            userModel.exist( response.uid ).then(
                                function( result ){
                                    if( result > 0){
                                        //update 
                                        userModel.update( userparameters );
                                    }else{
                                        //insert
                                        userModel.add( userparameters );
                                    }
                                }
                            );
                                    
                            /* ASSET */
                            /* set as deleted */
                            assetModel.flag ( response.uid );
                            
                            /* add/edit asset */
                            var assetparameters = null;
                            var assetdata = response.data.assets;
                                    
                            angular.forEach(assetdata, function(value, key) {                    
                                
                                assetparameters = {
                                                    id: value.id,
                                                    uid: response.uid,
                                                    code_reference: value.code_reference,
                                                    asset_name: value.asset_name,
                                                    location_id: value.location_id,
                                                    location_name: value.location_name,
                                                    team_id: value.team_id,
                                                    team_name: value.team_name,
                                                    category_id: value.category_id,
                                                    category_name: value.category_name,
                                                    latitude: value.latitude,
                                                    longitude: value.longitude,
                                                    barcode: value.barcode,
                                                    description: value.description,
                                                    address: value.address
                                                };
                                        
                                $scope.addOrUpdateAsset( assetparameters);
                                
                                   
                                /* ATTRIBUTES */
                                /* set as deleted */
                                attributeModel.flag ( value.id );
                                $scope.createAttributesParameters( value.attributes, assetparameters.id  );
                                
                                
                            });
                        }

                        $timeout(function(){
                            $state.go('app.assets');
                        }, 1000)
                    }        
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    });
                }
            }
        ); 
    };
    
    $scope.addOrUpdateAsset = function( assetparameters ){
        //console.log(assetparameters);
        assetModel.exist( assetparameters.id ).then(
            function( result ){
                if( result > 0){
                    //update 
                    assetModel.update ( assetparameters );
                }else{
                    //insert
                    assetModel.add ( assetparameters );
                }
            }
        );
    };
    
    $scope.createAttributesParameters = function( attributes, asset_id ){ 
        var attributesparameters = null;
        angular.forEach(attributes, function(value, key) {      
            attributesparameter = {
                id : value.attribute_value_id,
                asset_id : asset_id,
                asset_attributes_name : value.asset_attributes_name,
                attribute_value_name : value.attribute_value_name
            };
            
            $scope.addOrUpdateAttributes( attributesparameter, asset_id );
        });                                        
        
     };
    
    
    $scope.addOrUpdateAttributes = function( attributesparameter, asset_id ){
        //console.log(assetparameters);
        attributeModel.exist( attributesparameter.id, asset_id ).then(
            function( result ){
                if( result > 0){
                    attributeModel.update ( attributesparameter );
                }else{
                    //console.log('insert');
                    attributeModel.add ( attributesparameter );
                }
            }
        )
    };
})
    
.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
    
    $scope.isMobile = function(){
        if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
            return true;
        } else {
            return false;
        }
    }
    
    $scope.assetsView = null;

})

.controller('AssetsCtrl', function($scope, $state, $ionicHistory, 
                                    $timeout, $cordovaSQLite, $ionicPopup,
                                    $ionicPlatform, $cordovaBarcodeScanner,$q,
                                    ionicMaterialInk, 
                                    ionicMaterialMotion, 
                                    amtissData,
                                    assetModel,
                                    attributeModel) {
    
    
    
    var assetsAndType       = [];
    var assets              = [];
    $scope.data             = [];
    $scope.markers          = [];
    $scope.assetterm        = '';
    $scope.type             = 'all';
    $scope.centerLat        = null;
    $scope.centerLong       = null;
    $scope.view             = 'list';
    $scope.disabledMap      = false;
    
    $scope.init = function() {
        /* TODO: OPEN LATER remove unwanted asset */
        assetModel.removebyflag ( amtissData.getUid() );
        
        if( typeof google !== 'undefined' ){
            $scope.disabledMap        = true;
        }
        
        $scope.showList( 'all', '' );
        
        $scope.$on('ngLastRepeat.dataList',function(e) {
            ionicMaterialInk.displayEffect();
        });
        
    };
    
    $scope.setView = function( view ){
        $scope.view = view;
    }
    
    $scope.getListByFilter = function( type, term , name){
        
        var assetsByFilter = [];
        var uid = amtissData.getUid();
        //if( type === 'category' ){
            var qq = "SELECT * FROM assets WHERE ( uid = ? ) AND ( " + type + "_name = ? ) ORDER BY " + type + "_name ";
            var where = [uid, name];
                            
            if( term !== ''){
                qq = "SELECT * FROM assets WHERE ( uid = ? ) AND ( " + type + "_name = ? ) " 
                    + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                    + " ORDER BY " + type + "_name ";
                where = [uid, name, '%' + term + '%', '%' + term + '%', '%' + term + '%' , '%' + term + '%' , '%' + term + '%' , '%' + term + '%' , '%' + term + '%' ];
            } 

            $cordovaSQLite.execute(db, qq, where).then(function(subresult) {
                if(subresult.rows.length > 0) {
                    
                    for(var i=0;i<subresult.rows.length;i++){
                        var asset = subresult.rows.item(i);
                        assetsByFilter.push(asset);
                    }
                    
                    $scope.initializeMap( assetsByFilter );
//                    angular.forEach(subresult.rows,function(value,index){
//                        attributeModel.getbyasset( value.server_id ).then(
//                            function( subresult ){
//                                value.attributes = subresult;
//                                assetsByFilter.push( value );
//                            }
//                        );
//                    });
//                    for(var j=0;j<subresult.rows.length; j++){
//                        //assetsByFilter.push( subresult.rows.item(j) );
//                        
//                        
//                        var item = subresult.rows.item(j);
//                        //item.attributes = $scope.getAttributesByAsset( item.server_id );
//                        
//                        attributeModel.getbyasset( item.server_id ).then(
//                            function( subsubresult ){
//                                item.attributes = subsubresult;
//                                assetsByFilter.push( item );
//                            }
//                        );
//                
//                        if( subresult.rows.item(j).latitude != '' && subresult.rows.item(j).longitude != '' ){
//                            
//                            var mar = {
//                                            id: subresult.rows.item(j).code_reference,
//                                            latitude: subresult.rows.item(j).latitude,
//                                            longitude: subresult.rows.item(j).longitude,
//                                            title: subresult.rows.item(j).code_reference
//                                        };
//
//                                 mar.onClick = function() {
//                                    console.log("Clicked!");
//                                    //mar.show = !mar.show;
//                                };
//                            
//                            $scope.markers.push( mar );
//                            
//                            console.log( $scope.markers );
//                            
//                            //reset markup
//                            if( typeof google !== 'undefined' ){
//                                if( j === 0 && google !== null){
//
//                                    //$scope.centerLat = subresult.rows.item(j).latitude;
//                                    $scope.centerLong = subresult.rows.item(j).longitude;
////                                    console.log($scope.centerLat);
////                                    console.log($scope.centerLong);
//                                }
//                            }
//                        }
//                    }
                } else {
                    console.log("NO ROWS EXIST");
                }
            }, function(error) {
                console.error(error.message);
            });
        
        return assetsByFilter;
    };
    
    $scope.showList = function ( type, term ){  
        $scope.data.length      = 0;
        $scope.markers.length   = 0;
        
        $scope.type = type;
        
        var uid = amtissData.getUid();

        
        if( type === 'all'){    
            var q = "SELECT a.* " +
//                    " , '[' || " +
//                    "  GROUP_CONCAT( " +
//                    "        '{\"id\":' || " +
//                    "          b.id || " +
//                    "          ', \"asset_attributes_name\":\"' || " +
//                    "          b. `asset_attributes_name` || " + 
//                    "          '\", \"attribute_value_name\":\"' || " +
//                    "          b.attribute_value_name || " +
//                    "          '\", \"$$hashKey\":\"' || " +
//                    "          b.id || a.id || \"xxx\" || " +
//                    "          '\"}' " +
//                    "  ) || ']' "+
//                    "  AS attributes " +
                    " FROM assets AS a "+
                    " LEFT JOIN attributes AS b ON a.server_id = b.asset_id " +
                    " WHERE a.uid = ?" +
                    " GROUP BY a.server_id, a.code_reference ";
            var where = [uid];
                
            if( term !== '' ){
                q       = " SELECT * FROM assets WHERE (uid = ?)  AND "
                        + " ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) )";
                where   = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
                      
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                assets.length = 0;
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length;i++){
                        var asset = result.rows.item(i);
                        assets.push(asset);
                    }
                    
                    $scope.initializeMap( assets );
                    
//                    angular.forEach(result.rows,function(value,index){
//                        alert(value.server_id);
//                        attributeModel.getbyasset( value.server_id ).then(
//                            function( subresult ){
//                                value.attributes = subresult;
//                                assets.push( value );
//                            }
//                        );
//                    });
                } else {
                    var alertPopUp = $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: amtissData.getMessage( 'no_asset_found' )
                         });
                    return;
                }
                    
                $scope.data = [{'type':'All', 'assets': assets}];
                
            }, function(error) {
                console.error(error.message);
            });
        }else if( type === 'category' ){
            //console.log (' Type Category ');
            assetsAndType.length  = 0;
            
            //get available category sort by name
            var q = "SELECT DISTINCT category_name FROM assets WHERE ( uid = ? ) AND ( category_name IS NOT NULL ) ORDER BY category_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT category_name FROM assets WHERE ( uid = ? ) AND ( category_name IS NOT NULL ) " 
                    + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                    + " ORDER BY category_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
                            
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).category_name;           
                        assetsAndType.push({'type':name, 'assets':$scope.getListByFilter( type, term , name) });
                            
                        $scope.data = assetsAndType;
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_asset_found' )
                    });
                    return;
                }
            }, function(error) {
                console.error(error);
            });
        }else if( type === 'location' ){
            
            assetsAndType.length    = 0;
            
            //get available location sort by name
            var q = "SELECT DISTINCT location_name FROM assets WHERE ( uid = ? ) AND ( location_name IS NOT NULL ) ORDER BY location_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT location_name FROM assets WHERE ( uid = ? ) AND ( location_name IS NOT NULL ) " 
                    + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                    + " ORDER BY location_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
                
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).location_name;           
                        assetsAndType.push({'type':name, 'assets':$scope.getListByFilter( type, term , name) });
                            
                        $scope.data = assetsAndType;
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_asset_found' )
                    });
                    return;
                }
            }, function(error) {
                console.error(error);
            });
        }else if( type === 'team' ){
            
            assetsAndType.length    = 0;
            
            //if( $scope.$parent.isMobile() === true ) {
                //get available team sort by name
                var q = "SELECT DISTINCT team_name FROM assets WHERE ( uid = ? ) AND ( team_name IS NOT NULL ) ORDER BY team_name ";
                var where = [uid];
                
                if( term !== ''){
                    q = "SELECT DISTINCT team_name FROM assets WHERE ( uid = ? ) AND ( team_name IS NOT NULL ) " 
                        + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                        + " ORDER BY team_name ";
                    where = [uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
                }
                
                $cordovaSQLite.execute(db, q, where).then(function(result) {
                    if(result.rows.length > 0) {
                        for(var i=0;i<result.rows.length; i++){
                            var name = result.rows.item(i).team_name;           
                            assetsAndType.push({'type':name, 'assets':$scope.getListByFilter( type, term , name) });
                            //console.log( 'Name....: ' + name );
                            
                            $scope.data = assetsAndType;
                        }
                    } else {
                        //console.log("NO ROWS EXIST");
                        var alertPopUp = $ionicPopup.alert({
                              title: amtissData.getAppTitle(),
                              template: amtissData.getMessage( 'no_asset_found' )
                         });
                         return;
                    }
                }, function(error) {
                    console.error(error);
                });
                
                
                
            //}else{
            //    $scope.data = [ { 'type':'Location 1', 'assets': assetsData.list.slice(0,1) }, { 'type':'Location 2', 'assets': assetsData.list.slice(1,4) }];
             //   $scope.markers.length = 0;
                
//                for(var i=0;i<assetsData.list.slice(0,3).length;i++){
//                //console.log (i + '   ' +  assetsData.list[i] );
//                    $scope.markers.push({
//                        id: assetsData.list[i].code_reference,
//                        latitude: assetsData.list[i].latitude,
//                        longitude: assetsData.list[i].longitude,
//                        title: assetsData.list[i].code_reference
//                    })
//                }
            
            //}
        }
    };
    
    $scope.scan = function(){
         if( typeof cordova === 'undefined' ){
             return; 
         }
        
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner
            .scan()
            .then(function(imageData) {
                // Success! Barcode data is here
                //console.log( text );
                alert(imageData.text);
                alert("Barcode Format -> " + imageData.format);
                alert("Cancelled -> " + imageData.cancelled);
                
            }, function(error) {
                // An error occurred
                console.log( error );
                alert( 'error ' + error );
            });
        });
    };
        
    $scope.getAssetByAttribute = function( asset_id ){
        var q = "SELECT * FROM attributes WHERE asset_id = (?) ORDER BY asset_attributes_name ";
        var where = [asset_id];
        var arrAttr = [];
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            if( result.rows.length > 0 ){
                for(var i=0;i<result.rows.length; i++){
                    artAttr.push(result.rows.item(i));
                }
            }
            return arrAttr;
        }, function(error) {
            console.error(error);
            return arrAttr;
        });
        
        
    }
    
    $scope.initializeMap = function(assetsmap){
        
        if( assetsmap.length > 0 ){
            if( typeof $scope.map === 'undefined'){
                $scope.centerLat = assetsmap[0].latitude;
                $scope.centerLong = assetsmap[0].longitude;
                $scope.map = {center: {latitude: $scope.centerLat, longitude:$scope.centerLong }, zoom: 16 };
                $scope.options = {scrollwheel: false};
            }
        
       
            for(var i=0;i<assetsmap.length;i++){
                
                if( assetsmap[i].latitude != '' && assetsmap[i].latitude != '' ){
                    var mar = {
                                id: assetsmap[i].code_reference,
                                latitude: assetsmap[i].latitude,
                                longitude: assetsmap[i].longitude,
                                title: assetsmap[i].asset_name
                            };

                    mar.onClick = function() {
                        console.log("Clicked!");
                        //mar.show = !mar.show;
                    };
                    

                    $scope.markers.push( mar );
                }
            }
        
            $scope.map.center.latitude = assetsmap[0].latitude;
            $scope.map.center.longitude = assetsmap[0].longitude;
        }
    };
})

.controller('AssetsMapCtrl', function($scope, $state, $ionicHistory, $timeout,
                                       ionicMaterialInk, ionicMaterialMotion, assetsData) {
    
    $scope.init = function() {
        //console.log ( assetsData.view );
        $scope.term     = assetsData.term;
        $scope.view     = assetsData.view;
        $scope.views    = assetsData.views;
        $scope.filter   = assetsData.filter;
        $scope.filters  = assetData.filters;
        $scope.assets   = assetsData.list;  
        
        //$scope.$on('ngLastRepeat.dataList',function(e) {
        //    ionicMaterialInk.displayEffect();
        //});
    };
})



.controller('WorkordersCtrl', function($scope) {
})

.controller('MessagingCtrl', function($scope) {
})

.controller('GuidelineCtrl', function($scope) {
})

.controller('SettingsCtrl', function($scope) {
})


.controller('UserCtrl', function($scope) {
});
