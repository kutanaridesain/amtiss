angular.module('amtiss.users', [])


.service('userModel', function ( $cordovaSQLite ) {
    this.yes = 1, 
    this.no = 0,
    this.error = -1,
    this.isNew = function(server_id){
        var q = "SELECT * FROM employee WHERE server_id = ?";
        var where = [server_id];        

        $cordovaSQLite.execute(db, q, where).then(function(result) {
            if(result.rows.length === 0) {
                return 1;
            }else{
                return 0;
            }
        },
        function(error) {
            return -1;      //error
        });
        
       
        
    }
})

.factory('xxxuserModel', function ( $cordovaSQLite ) {
    
    return({
        status:false,
        selectByUsernamePassword: selectByUsernamePassword,
        selectById : selectById,
        setAsDelete : setAsDelete,
        isNew : isNew,
        insert: insert,
        update: update,
        x:function( server_id ){
         var q = "SELECT * FROM employee WHERE server_id = ?";
         var where = [server_id];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            if(result.rows.length === 0) {
                console.log('okkkkk. ');
                $this.status = true;   //ada
            }else{
                return this.error;   //tidak
            }
        },
        function(error) {
            console.error('User check : ' + error.message);
            for (var key in error) {
              if (error.hasOwnProperty(key)) {
                console.log('LOGGGG ' + key + " -> " + error[key]);
              }
            }
            return -1;      //error
        });
    }
    });
    
    function selectByUsernamePassword( username, password ){
         var q = "SELECT * FROM employee WHERE username = ? AND password = ? ";
         var where = [username, password];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            return result;
        },
        function(error) {
            console.error('User ' + error.message);
            return -1;
        });
    }
    
    function selectById( server_id ){
         var q = "SELECT * FROM employee WHERE server_id = ?";
         var where = [server_id];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            if(result.rows.length > 0) {
                return  result;
            }else{
                return 0;
            }
        },
        function(error) {
            console.error('User ' + error.message);
            return -1;
        });
    }
    
    function setAsDelete( uid ){
        var q = "UPDATE employee set is_delete = ? WHERE uid = ? ";
            
        var params = [ 'Y', uid ];
            
        $cordovaSQLite.execute(db, q, params).then(function(result) {
            return 1;
        }, function(error) {
            console.error('User ' + error.message);
            return -1;
        });
    }
    
    function isNew( server_id ){
         var q = "SELECT * FROM employee WHERE server_id = ?";
         var where = [server_id];
        
        $cordovaSQLite.execute(db, q, where).then(function(result) {
            console.log( 'Length '+ result.rows.length);
            if(result.rows.length === 0) {
                console.log('new');
                return this.ok;   //ada
            }else{
                return this.error;   //tidak
            }
        },
        function(error) {
            console.error('User check : ' + error.message);
            for (var key in error) {
              if (error.hasOwnProperty(key)) {
                console.log('LOGGGG ' + key + " -> " + error[key]);
              }
            }
            return -1;      //error
        });
    }
    
    function insert( data ){
        
        console.log('START INSERT ......');
        
        var q = "INSERT INTO employee (server_id, fullname, username, password, company_name, picture, is_delete ) " +
                "VALUES ( ?, ?, ?, ?, ?, ?, ? )";
            
        var params = [  data.uid, data.fullname, data.username, data.password, data.company_name,
                        data.picture, 'N'];
            
        $cordovaSQLite.execute(db, q, params).then(function(result) {
            return result.insertId;
        }, function(error) {
            console.error('User insert : ' + error.message);
            for (var key in error) {
              if (error.hasOwnProperty(key)) {
                console.log('LOGGGG ' + key + " -> " + error[key]);
              }
            }
            return -1;
        });
    }
    
    function update( data ){
        var q = "UPDATE employee set server_id = ?, fullname = ?, username = ?, password = ?, " +
                "company_name = ?, picture = ?, is_delete = ? WHERE server_id = ? ";
            
        var params = [ data.uid, data.fullname, data.username, data.password, data.company_name,
                        data.picture, 'N', data.uid];
            
        $cordovaSQLite.execute(db, q, params).then(function(result) {
            return data.uid;
        }, function(error) {
            console.error('User ' + error.message);
            return -1;
        });
    }
    
});