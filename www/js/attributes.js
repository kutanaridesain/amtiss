angular.module('amtiss.attributes', [])

.factory('attributeModel', function($cordovaSQLite, $q, DBA, amtissData) {
    var self = this;
    self.sync = false;
    
    self.startingOfflineId = amtissData.getStartingOfflineId();
    
    self.flag = function( asset_id ) {
        var parameters  = ['Y', 'O', asset_id];
        //console.log( parameters );
        var query       = "UPDATE attributes set is_delete = ? "+
                          "WHERE action = ? AND asset_id = ? ";
        
        return DBA.query(query, parameters);
    }
    
    /* array */
    self.createAttributesParameters = function( attributes, asset_id, sync ){ 
        var attributesparameters = null;
        self.sync = sync;
        angular.forEach(attributes, function(value, key) { 
                var name = value.asset_attributes_name;

                if( typeof( name )  === 'undefined' ){
                    //console.log( value );
                    self.getundefinedname( value.attribute_value_asset ).then(function( result ){
                        name = result.name;
                        //console.log( result );
                        //var server_id = typeof( result.server_id )==='undefined'?(new Date().valueOf()):result.server_id;
                        var server_id = typeof( value.server_id )==='undefined'?(new Date().valueOf()):value.server_id;
                        attributesparameter = {
                            server_id : server_id,
                            asset_id : asset_id,
                            asset_attributes_name : name,
                            attribute_value_name : value.attribute_value_name,
                            attribute_value_asset : value.attribute_value_asset,
                            action : typeof( value.action ) === 'undefined'?'O':value.action
                        };

                        if( value.action !== 'D'){
                            self.addOrUpdateAttributes( attributesparameter, asset_id );
                        }else{
                            self.flagbyserverid(server_id);
                        }
                    });
                }else{
                    var server_id = typeof( value.attribute_value_id )==='undefined'?(new Date().valueOf()):value.attribute_value_id;
                    attributesparameter =  {
                        server_id : server_id,
                        asset_id : asset_id,
                        asset_attributes_name : name,
                        attribute_value_name : value.attribute_value_name,
                        attribute_value_asset : value.attribute_value_asset,
                        action : typeof( value.action ) === 'undefined'?'O':value.action
                    };

                    if( value.action !== 'D'){
                        self.addOrUpdateAttributes( attributesparameter, asset_id );
                    }else{
                         self.flagbyserverid(server_id);
                    }
                }
            //}
        });
    };
    
    /* single */
    self.createAttributeParameters = function( attribute, asset_id, sync ){ 
        var attributesparameters = null;
        self.sync = sync;
        
        var name = attribute.asset_attributes_name;

        if( typeof( name )  === 'undefined' ){
            self.getundefinedname( Number(attribute.attribute_value_asset) ).then(function( result ){
                name = result.name;
                
                attributesparameter = {
                            server_id : Number(attribute.server_id),
                            asset_id : Number(asset_id),
                            asset_attributes_name : name,
                            attribute_value_name : attribute.attribute_value_name,
                            attribute_value_asset : attribute.attribute_value_asset,
                            action : typeof( attribute.action ) === 'undefined'?'O':attribute.action
                };

                if( attribute.action !== 'D'){
                    self.addOrUpdateAttributesOnline( attributesparameter, asset_id );
                }else{
                    self.removebyserverid(attribute.server_id);
                }
            });
        }else{
            attributesparameter =  {
                        server_id : Number(attribute.server_id),
                        asset_id : Number(asset_id),
                        asset_attributes_name : name,
                        attribute_value_name : attribute.attribute_value_name,
                        attribute_value_asset : attribute.attribute_value_asset,
                        action : typeof( attribute.action ) === 'undefined'?'O':attribute.action
            };

            if( attribute.action !== 'D'){
                self.addOrUpdateAttributesOnline( attributesparameter, asset_id );
            }else{
                 self.removebyserverid(attribute.server_id);
            }
        }
    };
    
     self.flagbyserverid = function( server_id ) {
        if( server_id > self.startingOfflineId ){
            var parameters = [server_id];
            return DBA.query("DELETE FROM attributes WHERE server_id = (?)", parameters);
        }else{
            var parameters  = ['Y', 'D', server_id];
            var query       = "UPDATE attributes set is_deleted = ? AND action = ? "+
                              "WHERE server_id = ? ";
            return DBA.query(query, parameters);
        }
    }
    
    self.addOrUpdateAttributes = function( attributesparameter, asset_id ){
        //console.log( attributesparameter) 
        self.exist( attributesparameter.server_id ).then(
            function( result ){
                if( result > 0){
                    if( attributesparameter.server_id > self.startingOfflineId ){
                         attributesparameter.action = 'C';
                     }
                    self.update ( attributesparameter );
                }else{
                    //console.log( 'insert' );
                    self.add ( attributesparameter );
                }
            }
        )
    };
    
    self.addOrUpdateAttributesOnline = function( attributesparameter, asset_id ){
        //console.log( attributesparameter)
        self.exist( attributesparameter.server_id ).then(
            function( result ){
                if( result > 0){
                    //console.log( 'update' );
                    self.update ( attributesparameter );
                }else{
                    //console.log( 'insert' );
                    self.add ( attributesparameter );
                }
            }
        )
    };
    
    self.exist = function(server_id) {
        var parameters = [server_id];
        //console.log( server_id );
        return DBA.query("SELECT id FROM attributes WHERE server_id = (?) ", parameters)
        .then(function(result) {
            return result.rows.length;
        });
    }
    
    self.add = function( data) {
        //console.log( 'insert attrs' );
//        data.action = (self.startingOfflineId > data.asset_id ? 'O':'C');
        var parameters = [ data.server_id, data.asset_id, data.asset_attributes_name, data.attribute_value_name, data.attribute_value_asset, data.action, 'N'];
        
        var query      = "INSERT INTO attributes (server_id, asset_id, asset_attributes_name, attribute_value_name, attribute_value_asset, " +
                         "action, is_delete ) VALUES (?,?,?,?,?,?,?)";
        
        return DBA.query( query, parameters );
    }
    
    self.update = function( data ) {
        //console.log( 'update attrs' );
        var parameters  = [ data.server_id, data.asset_id, data.asset_attributes_name, 
                           data.attribute_value_name, data.attribute_value_asset, data.action, 
                           'N', data.server_id ];
        var query       = "UPDATE attributes set server_id = ?, asset_id = ?, asset_attributes_name = ?, attribute_value_name = ?, " +
                          "attribute_value_asset = ?, " +
                          "action = ?, is_delete = ? WHERE server_id = ? ";
        
        if( self.sync === true ){
            var parameters  = [ data.server_id, data.asset_id, data.asset_attributes_name, 
                               data.attribute_value_name, data.attribute_value_asset, data.action, 
                               'N', data.server_id, 'O' ];
            var query       = "UPDATE attributes set server_id = ?, asset_id = ?, asset_attributes_name = ?, attribute_value_name = ?, " +
                          "attribute_value_asset = ?, " +
                          "action = ?, is_delete = ? WHERE server_id = ? AND action = ? ";
        
        }
        
        return DBA.query(query, parameters); 
    }
    
    self.updateOfflineData = function( parameters ){
         var query       = "UPDATE attributes set server_id = ?, asset_id = ?, asset_attributes_name = ?, attribute_value_name = ?, " +
                                                  "attribute_value_asset = ?, " +
                                                "action = ?, is_delete = ? WHERE server_id = ? ";
        return DBA.query(query, parameters); 
    }
    
    self.removebyflag = function( asset_id ) {
        var parameters = [asset_id];
        return DBA.query("DELETE FROM attributes WHERE action = 'O' AND is_delete = 'Y' AND asset_id = (?)", parameters);
    }
    
    self.removebyserverid = function( server_id ) {
        var parameters = [server_id];
        return DBA.query("DELETE FROM attributes WHERE  server_id = (?)", parameters);
    }
    
    self.getbyassetwithindex = function( asset_id , i) {
        var parameters = [asset_id];
        return DBA.query("SELECT * FROM attributes WHERE asset_id = (?) AND is_delete = 'N' ORDER BY asset_attributes_name LIMIT 3 OFFSET 0 ", parameters)
        .then(function(result) {
            var attributes = DBA.getAll(result);
            return {list:attributes, index: i};
        });
    }

    self.getbyasset = function( asset_id ) {
        var parameters = [asset_id];
        return DBA.query("SELECT * FROM attributes WHERE asset_id = (?) AND is_delete = 'N' "+
                         " ORDER BY asset_attributes_name ", parameters)
        .then(function(result) { 
            return DBA.getAll(result);
        });
    }
    
    self.autocomplete = function(  ) {
        var parameters = [];
        var q = "SELECT DISTINCT asset_attributes_name as name, server_id as id, asset_attributes_name as view FROM attributes WHERE is_delete <> 'Y' AND action <> 'C' ";
        
        return DBA.query(q, parameters)
        .then(function(result) {
            return DBA.getAll(result);
        });
    }
    
    self.getundefinedname = function( id ) {
        var parameters = [id];
        console.log( parameters );
        return DBA.query("SELECT name FROM atts WHERE server_id = (?) ", parameters)
        .then(function(result) {
            //console.log( DBA.getById( result ) );
            var name = '';
            if( result.rows.length > 0 ){
                name = result.rows.item(0).name;
            }
            return {'id':id, 'name':name};
        });
    }
    
    self.updateOfflineId = function( online_id, offline_id, asset_id ) {
        var parameters  = [ online_id, asset_id, 'O', offline_id ];
        console.log( parameters );
        var query       = "UPDATE attributes set server_id = ?, asset_id = ?, action = ? "+
                          "WHERE server_id = ? ";
        return DBA.query(query, parameters);
    }
    
     self.getallbyasset = function( asset_id ) {
        var parameters = [asset_id];
        return DBA.query("SELECT * FROM attributes WHERE asset_id = (?) "+
                         " ORDER BY asset_attributes_name ", parameters)
        .then(function(result) { 
            return DBA.getAll(result);
        });
    }
    
    return self;
})