angular.module('amtiss.controllers', [])

.controller('LoginCtrl', function($scope, $ionicPopup,
                                    $state, $timeout,
                                    $cordovaSQLite, 
                                    $ionicPlatform,
                                    $base64, $q,
                                    amtissService,
                                    amtissData, 
                                    assetModel, 
                                    userModel,
                                    attributeModel, 
                                    locationModel,
                                    categoryModel,
                                    companyModel,
                                    vendorModel,
                                    teamModel,
                                    attModel, 
                                    assetSurveyTemplateModel,
                                    assetSurveyModel,
                                    assetImagesModel,
                                    workOrderModel,
                                    workorderImagesModel,
                                    LocalStorage,
                                    $q
                                  ) {
    $scope.data = [];
    
    $scope.dropTable = function (){
            var q = "DROP TABLE user";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE assets";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE attributes";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE locations";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE categories";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE companies";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE vendors";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE teams";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE atts";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE asset_images";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE user_input";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE survey";
            $cordovaSQLite.execute(db, q);
            
            q = "DROP TABLE survey_template";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE workorders";
            $cordovaSQLite.execute(db, q);
            
            q = "DROP TABLE workorder_images";
            $cordovaSQLite.execute(db, q);
        
            $state.go('login');        
    }
    
    $scope.init = function() {
        $scope.data.username = 'admin';
        $scope.data.password = 'in0t3ch';
        $scope.data.password = '4mti55';  
    }
    
    $scope.validateLogin = function( isValid ){
        if( isValid ){
            $ionicPlatform.ready(function() {
                if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
                    if(window.Connection) {
                        if(navigator.connection.type == Connection.NONE) {
                            $scope.loginlocal();
                        }else{
                            $scope.login();
                        }
                    }else{
                        $scope.loginlocal();
                    }
                }else{
                    if( navigator.onLine === false ) {
                        $scope.loginlocal();
                    }else{
                        $scope.login();
                    }
                } 
            });
        }
    }
    
    $scope.loginlocal = function(){
        var username64 = $base64.encode($scope.data.username);
        var password64 = $base64.encode($scope.data.password);
        
        userModel.getbyaccount( username64, password64 ).then(
            function( result ){
                if( result != null ){
                    //TODO REMOVE HARCODE
                    amtissData.setToken( 'offline token' );
                    amtissData.setUid( result.server_id );
                    $state.go('app.assets', {actionType:0});
                    //$state.go('action', {actionId:0});
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: amtissData.getMessage('not_valid_user')
                    });
                    
                }
            }
        );
    }
    
    $scope.login = function() {
    
        $scope.param = { isajax:1, username:$scope.data.username, password:$scope.data.password, key: 'amtiss' };

        amtissService.doRequest( 'login', $scope.param, null, 4600 )
        .then(
            function( response ) {                 
                if( response.status === true ){
                    
                    if( typeof response.data !== 'undefined'){            
                        if( typeof response.data.assets !== 'undefined'){
                            //set token and uid
                            
                            if( response.user !== null ){
                                amtissData.setToken( response.token );
                                amtissData.setUid( response.user.uid );
                                amtissData.setUsername( response.user.name );
                                amtissData.setCompany( response.user.company );

                                /* USER */
                                var usernameMD5 = $base64.encode($scope.data.username);
                                var passwordMD5 = $base64.encode($scope.data.password);

                                var userparameters = { uid:amtissData.getUid(), fullname:$scope.data.username , username:usernameMD5, 
                                                       password:passwordMD5, company_name: amtissData.getCompany(), picture:null };

                                userModel.exist( amtissData.getUid() ).then(
                                    function( result ){
                                        if( result > 0){
                                            //update 
                                            userModel.update( userparameters );
                                        }else{
                                            //insert
                                            userModel.add( userparameters );
                                        }
                                    }
                                );

                                /* ASSET */
                                /* set as deleted */
                                assetModel.flag ( amtissData.getUid() ).
                                then(function( result ){

                                    /* add/edit asset */
                                    var assetparameters = null;
                                    var assetdata = response.data.assets;
                                    $scope.data.workorders = response.data.workorders;
                                    
                                    var q = $q.defer();
                                    var promises = [];

                                    angular.forEach(assetdata, function(value, key) {                    

                                        assetparameters = {
                                                            server_id: value.id,
                                                            uid: amtissData.getUid(),
                                                            code_reference: value.code_reference,
                                                            asset_name: value.asset_name,
                                                            location_id: value.location_id,
                                                            location_name: value.location_name,
                                                            team_id: value.team_id,
                                                            team_name: value.team_name,
                                                            category_id: value.category_id,
                                                            category_name: value.category_name,
                                                            parent_id: value.parent_id, 
                                                            parent_name: value.parent_name,
                                                            company_id: value.company_id,
                                                            company_name: value.company_name,
                                                            latitude: value.latitude,
                                                            longitude: value.longitude,
                                                            barcode: value.barcode,
                                                            description: value.description,
                                                            address: value.address,
                                                            create_date: value.create_date,
                                                            survey_id: value.survey_id,
                                                            action: 'O',
                                                            sync: true
                                                        };                                        

                                        promises.push(assetModel.addOrUpdateAsset( assetparameters ));    
                                     });
                                    
                                    $q.all(promises).then(function() {
                                        //console.log('delete will be here');
                                        assetModel.removebyflag( amtissData.getUid() ).then(function(){                                            
                                            angular.forEach(assetdata, function(value, key) {   
                                                /* ATTRIBUTES */
                                                /* set as deleted */     
                                                attributeModel.flag ( value.id ).then(function(){
                                                    attributeModel.createAttributesParameters( value.attributes, value.id, true  );
                                                });                                            
                                             });    
                                            
                                            $scope.syncAssetImages();
                                        })
                                    });                                    
                                    
                                    /* SYNC WO */
                                    $scope.doSyncWO();
                                    /* MASTER TABLE */
                                    $scope.syncronizeMasterTable();
                                });
                            }
                        } 
                    }        
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    });
                }
            }
        ); 
    };
    
    
    $scope.syncAssetImages = function(){
        $ionicPlatform.ready(function() {
            if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
                if(window.Connection) {
                    if(navigator.connection.type !== Connection.NONE) {
                       $scope.dosyncAssetImages();
                       $scope.dosyncAssetSurvey();
                    }
                }else{
                     $timeout(function(){
                        $state.go('app.assets', {actionType:0});
                        //$state.go('action', {actionId:0})
                    }, 3000);
                }
            }else{
                if( navigator.onLine === true ) {
                    $scope.dosyncAssetImages();
                    $scope.dosyncAssetSurvey();
                }else{
                     $timeout(function(){
                        $state.go('app.assets', {actionType:0});
                        //$state.go('action', {actionId:0})
                    }, 3000);
                }
            } 
        });
    }
    
    $scope.dosyncAssetImages = function(){
        //get asset if form local
        var q = $q.defer();
        var promises = [];
        
        assetModel.getuniqueassets(amtissData.getUid()).then(function(result){
            //console.log( result );
            angular.forEach(result, function(value, key) {   
                //console.log( value );
                promises.push($scope.doSyncImageByAssetId( value.server_id ));
            });
        });
        
        
        $q.all(promises).then(function() {
            //$scope.doSyncWO();
        });
        
    }
    
    $scope.doSyncImageByAssetId = function( id ){
        $scope.param = {'isajax':1, 'token': amtissData.getToken(), 'assetID':id };
        amtissService.doRequest( 'getassetimages', $scope.param, true )
        .then(
            function( response ) { 
                //console.log( response );
                if( response.status == true ){
                    if( typeof(response.data.attachments) !== 'undefined' ){
                        var pictures = response.data.attachments

                        var defer = $q.defer();
                        var promises = [];
                        angular.forEach(pictures, function(value, key) { 
                            promises.push( assetImagesModel.async( value, amtissData.getUid(), id ) );
                        });

                        $q.all(promises).then(function() {
                            assetImagesModel.removebyflag().then(function( result ){
                               
                            });
                        });
                    }
                }
            }
        );
    }
    
    $scope.dosyncAssetSurvey = function(){
        //get asset if form local
        var q = $q.defer();
        var promises = [];
        
        assetModel.getuniqueassets(amtissData.getUid()).then(function(result){
            //console.log( result );
            angular.forEach(result, function(value, key) {   
                //console.log( value );
                promises.push($scope.doSyncSurveyByAssetId( value.server_id ));
            });
        });
        
        
        $q.all(promises).then(function() {
            //$scope.doSyncWO();
        });
        
    }
    
    $scope.doSyncSurveyByAssetId = function( assetId ){
        
        var token = amtissData.getToken();
        
        $scope.param = {'isajax':1, 'token':token, 'assetID': assetId };
        
        amtissService.doRequest( 'getassetsurveys', $scope.param, true )
        .then(
            function(  response ) { 
                //console.log( response );
                if( response.status === true ){
                    //console.log(response.data);
                     assetSurveyModel.flagbyassetid( assetId ).then(function(){
                        var inspections = response.data.inspections;
                        //console.log( inspections );
                        var q = $q.defer();
                        var promises = [];
                         
                        if( inspections != null ){
                            for( var i=0;i<inspections.length;i++ ){
                                //console.log( inspections[i].server_id );
                                //console.log( inspections[i] );
                                promises.push( assetSurveyModel.sync( inspections[i] ) );
                            }
                        }
                         
                         $q.all(promises).then(function() {
                            $scope.data = inspections; 
                            assetSurveyModel.removebyflag();
                         })
                    });
                }
            }
        );
    }
    
    $scope.doSyncWO = function(){
        
        workOrderModel.sync($scope.data.workorders, function(){
            $scope.syncWOImages();
        });
        
        $timeout(function(){
            $state.go('app.assets', {actionType:0});
            
        }, 3000);
    }
    
    $scope.syncWOImages = function(){
        //get asset if form local
        var uid = amtissData.getUid();
        workorderImagesModel.flagbyuid(amtissData.getUid()).then(function(){
            $scope.param = {'isajax':1, 'token': amtissData.getToken(), 'uid':uid };
            amtissService.doRequest( 'getworkorderimages', $scope.param, true )
            .then(
                function( response ) { 
                    //console.log( response );
                    if( response.status == true ){
                        var attachments = response.data.attachments;
//                        console.log(  attachments );
//                        console.log( typeof( attachments ));
                        //if( typeof( attachments ) !== 'undefined' ){
                            if( attachments !== null ){
                                var q = $q.defer();
                                var promises = [];
                                //console.log( 'start');
                                for(var i=0;i<attachments.length;i++){
                                    promises.push(workorderImagesModel.async(attachments[i], uid ));
                                //    console.log( 'for' );
                                }
                                
                                $q.all(promises).then(function() {
                                 //   console.log('all');
                                 })
                            }else{
                                workorderImagesModel.removebyflag();
                            }   
                        //}
                    }
                }
            );
        });
    }
    
    $scope.syncronizeMasterTable = function(){
        
        $scope.param = { isajax:1, token: amtissData.getToken() };
        
        /* LOCATION */
        /* set as deleted */
        locationModel.flag ();

        amtissService.doRequest( 'locations', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                var locationparameters = null;
                var locationdata = response.data;
                //console.log( response.data ) ;
                
                angular.forEach( locationdata, function( value, key ){
                    if( typeof (value) !== 'undefined' ){
                        locationparameters = {
                            id: value.id,
                            name: value.name,
                            action: 'O',
                            is_delete: 'Y'
                        }
                        locationModel.exist( value.id, locationparameters );
                    }
                });

            }
        );
        
        /* CATEGORY */
        /* set as deleted */
        categoryModel.flag ();

        amtissService.doRequest( 'categories', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                var categoryparameters = null;
                var categorydata = response.data;
                //console.log( response.data ) ;
                
                angular.forEach( categorydata, function( value, key ){
                    if( typeof (value) !== 'undefined' ){
                        categoryparameters = {
                            id: value.id,
                            name: value.name,
                            action: 'O',
                            is_delete: 'Y'
                        }
                        categoryModel.exist( value.id, categoryparameters );
                    }
                });

            }
        );
        
        /* COMPANY */
        /* set as deleted */
        companyModel.flag ();

        amtissService.doRequest( 'companies', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                var companyparameters = null;
                var companydata = response.data;
                angular.forEach( companydata, function( value, key ){
                    
                    if( typeof (value) !== 'undefined' ){
                        companyparameters = {
                            id: value.id,
                            name: value.name,
                            action: 'O',
                            is_delete: 'Y'
                        }
                        
                        companyModel.exist( value.id, companyparameters );
                    }
                });

            }
        );
        
        /* TEAM */
        /* set as deleted */
        teamModel.flag ();

        amtissService.doRequest( 'teams', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                var teamparameters = null;
                var teamdata = response.data;
                
                angular.forEach( teamdata, function( value, key ){
                    if( typeof (value) !== 'undefined' ){
                        teamparameters = {
                            id: value.id,
                            name: value.name,
                            action: 'O',
                            is_delete: 'Y'
                        }
                        teamModel.exist( value.id, teamparameters );
                    }
                });

            }
        );
        
        /* Vendors */
        /* set as deleted */
        vendorModel.flag ();

        amtissService.doRequest( 'partners', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                var vendorparameters = null;
                var vendordata = response.data;
                
                angular.forEach( vendordata, function( value, key ){
                    if( typeof (value) !== 'undefined' ){
                        vendorparameters = {
                            id: value.id,
                            name: value.name,
                            action: 'O',
                            is_delete: 'Y'
                        }
                        vendorModel.exist( value.id, vendorparameters );
                    }
                });

            }
        );
        
        /* ATTS */
        /* set as deleted */
        vendorModel.flag ();

        amtissService.doRequest( 'attributes', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                var attparameters = null;
                var attdata = response.data;
                
                angular.forEach( attdata, function( value, key ){
                    if( typeof (value) !== 'undefined' ){
                        attparameters = {
                            id: value.id,
                            name: value.name,
                            action: 'O',
                            is_delete: 'Y'
                        }
                        attModel.exist( value.id, attparameters );
                    }
                });

            }
        );
        
         
        /* SURVEY TEMPLATE */
        /* set as deleted */
        assetSurveyTemplateModel.flag ();

        amtissService.doRequest( 'surveys', $scope.param )
        .then(
            function( response ) { 
                /* add/edit */
                
                if( response.status === true ){
                    var tempparameters = null;

                    if( typeof( response.data.surveys ) !== 'undefined' ){

                        angular.forEach( response.data.surveys, function( value, key ){
                            if( typeof (value) !== 'undefined' ){
                                //console.log( value );
                                //console.log( value.server_id );
                                tempparameters = {
                                    id: value.server_id,
                                    survey_id: value.survey_id,
                                    json: LocalStorage.set(value),
                                    action: 'O',
                                    is_delete: 'Y'
                                }
                                assetSurveyTemplateModel.exist( value.server_id, tempparameters );
                            }
                        });
                    }
                    
                }
            }
        );
    }
})


//.controller('ActionCtrl', function( $scope, $rootScope, $state, $stateParams){
//    var action = $stateParams.actionId;
//    console.log( action )
//    if( action == 0 ){
//        $rootScope.type = 'a';
//        $state.go('app.assets');
//        return;
//    }else{
//        $rootScope.type = 'w';
//        $state.go('app.workorders');
//        return;
//    }
//})
    
.controller('AppCtrl', function($scope, $rootScope, $ionicPopup, $ionicModal, $timeout, $cordovaBarcodeScanner, 
                                 $ionicPlatform, $cordovaGeolocation, $q, assetModel, 
                                 assetSurveyModel, amtissData, amtissService) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
    
    $rootScope.assets = [];
    
    $scope.isOnline = function(){
        if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
            if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }else{
            if( navigator.onLine === false ) {
                return false;
            }else{
                return true;
            }
        } 
    }
    
    $scope.isMobile = function(){
        if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
            return true;
        } else {
            return false;
        }
    }
    
    $scope.doScan = function( action ){
         if( typeof cordova === 'undefined' ){
             return; 
         }
        
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner
            .scan()
            .then(function(imageData) {
                if( !imageData.calleled ){
                    if( action === 'edit' ){
                        assetModel.getbybarcode( imageData.text, amtissData.getUid() ).then(
                            function(result){
                                console.log( result );
                                return result;
                            }
                        );
                    }else{
                        console.log(imageData.text);
                        return imageData.text;
                    }
                }
                
            }, function(error) {
                // An error occurred
                console.log( error );
                alert( 'error ' + error );
            });
        });
    };
    
    $scope.getPosition = function(){
        var coord = {'lat':0, 'lng':0 };
        var q = $q.defer();
        var posOptions = {timeout: 10000, enableHighAccuracy: true};
        $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
            //console.log( position.coords );
            coord = {'lat':position.coords.latitude, 'lng':position.coords.longitude };
            q.resolve(coord);
        }, function(err) {
            //console.log( err );
            //q.reject(coord);
             var alertPopUp = $ionicPopup.alert({
                                                title: amtissData.getAppTitle(),
                                                template: 'Geotangging: '+ err.message
                                                });
            q.resolve(coord); 
        });
                
        return q.promise;
    }
    
    $scope.syncSurvey = function( token, assetId ){
        if( $scope.isOnline() === false ){
            return;
        }
        
        $scope.param = {'isajax':1, 'token':token, 'assetID': assetId };
        
        amtissService.doRequest( 'getassetsurveys', $scope.param, true )
        .then(
            function(  response ) { 
                //console.log( response );
                if( response.status === true ){
                    //console.log(response.data);
                     assetSurveyModel.flagbyassetid( assetId ).then(function(){
                        var inspections = response.data.inspections;
                         console.log( inspections );
                        var q = $q.defer();
                        var promises = [];
                         
                        if( inspections != null ){
                            for( var i=0;i<inspections.length;i++ ){
                                //console.log( inspections[i].server_id );
                                promises.push( assetSurveyModel.sync( inspections[i] ) );
                            }
                        }
                         
                         $q.all(promises).then(function() {
                            $scope.data = inspections; 
                            assetSurveyModel.removebyflag();
                         })
                    });
                }
            }
        );
    }
    
    $scope.assetsView = null;

})


.controller('AssetsCtrl', function($scope, $rootScope, $state, $stateParams, $ionicHistory, 
    $timeout, $cordovaSQLite, $ionicPopup, $ionicLoading, $ionicPlatform, 
    $cordovaBarcodeScanner, $q, ionicMaterialInk, ionicMaterialMotion, amtissData, 
    assetModel, attributeModel, attModel, workOrderModel, assetImagesModel, amtissService,
    LocalStorage, uiGmapGoogleMapApi, $ionicPopover, $ionicFilterBar) {
    
    var assetsAndType       = [];
    var assets              = [];
    $scope.data             = [];
    $scope.markers          = [];
    $scope.assetterm        = '';
    $scope.type             = 'all';
    $scope.centerLat        = null;
    $scope.centerLong       = null;
    $scope.view             = 'list';
    $scope.disabledMap      = true;
    $scope.firstLoad        = false;
    
    //Context menu
    $ionicPopover.fromTemplateUrl('templates/assetFilter.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
        popover.hide();
    });
    
    //Filter bar
    var filterBarInstance;
    $scope.showFilterBar = function () {
        filterBarInstance = $ionicFilterBar.show({
            items: $scope.items,
            update: function (filteredItems, filterText) {
                $scope.items = filteredItems;
                if (filterText) {
                    //console.log(filterText);
                    $scope.showList('all',filterText,'');
                }
            }
        });
    };
        
    //Swipe to refresh
    $scope.refreshItems = function () {
        $scope.init();
        if (filterBarInstance) {
            filterBarInstance();
            filterBarInstance = null;
        }

        $timeout(function () {
            //getItems();
            //console.log( new Date().toString() );
            $scope.$broadcast('scroll.refreshComplete');
        }, 1000);
    };
    
    $scope.init = function() {
    
//        $scope.$parent.getPosition().then(function( position ){
//            console.log( position );
//        });
        
        
        $rootScope.actionType = $stateParams.actionType;
        
        $scope.startingOfflineId = amtissData.getStartingOfflineId();
            
        $scope.disabledMap = true;
        uiGmapGoogleMapApi.then(function(maps) {
            if( typeof(google) !== 'undefined' ){
                $scope.disabledMap = false;
            }
        });
        $timeout(function(){
            /* TODO: LATER remove unwanted asset */
            //assetModel.removebyflag ( amtissData.getUid() );
            $scope.showList( 'all', '', '' );//type, term, scan
            $ionicLoading.show({});
        }, 200);

        $timeout( function(){
            $ionicLoading.hide();
            //console.log(  $scope.data );
        }, 2000);
        
        
        var isFirstLoad = JSON.parse(localStorage.getItem('firstload'));
            
        if( isFirstLoad === true ){//TODO: ADA KEMUNGKINAN PENYEBAB CRASH
            localStorage.setItem('firstload', JSON.stringify(false));
            $timeout( function(){
                $scope.bindPictures(10000);
            }, 10000);

            $timeout( function(){
                $scope.bindPictures(15000);
            }, 15000);
            
            $timeout( function(){
                $scope.bindPictures(20000);
            }, 20000);
        }
    };
    
    $scope.bindPictures = function(delay){
        if( typeof( $scope.data[0]) !== 'undefined'){
            if( $scope.data[0].assets.length > 0 ){
                for( var i=0;i<$scope.data[0].assets.length;i++ ){                    
                    assetImagesModel.getpicturebyassetid($scope.data[0].assets[i].server_id, i )
                    .then( function( data ){
                        if( typeof( data.picture[0]) !== 'undefined' ){
                            $scope.data[0].assets[data.index].src =  data.picture[0].base64;
                        }
                    });
                }
            }
        }
    }
    
    $scope.setView = function( view ){
        $scope.view = view;
    };
    
    $scope.showListByWO = function ( type, term, barcode ){        
        $scope.data.length      = 0;
        $scope.markers.length   = 0;
        
        term = term.toLowerCase();
        
        $scope.type = type;
        
        if( barcode !== ''){
            term = '';
            $scope.assetterm = term;
        }
        
        var uid = amtissData.getUid();

        var workorder = null;
        var wo_asset  = null;
        var sub_wo_asset = null;
        workOrderModel.getbyid($rootScope.actionType).then( function( data ){
            workorder = data;
            if ( workorder !== null ) {
                wo_asset = LocalStorage.get( workorder.json );
                //console.log( wo_asset );
                sub_wo_asset = wo_asset;
                if( type === 'all'){
                    var arrAssets = [];
                    for(var j=0;j<sub_wo_asset.length;j++){
                        //console.log(sub_wo_asset);
                        sub_wo_asset[j].src='img/ionic.png';
                        assetImagesModel.getbyassetwithindex( sub_wo_asset[j].asset_id, j ).then(
                            function( subresult ){
                                if( subresult.picture.length === 1 ) {
                                    sub_wo_asset[subresult.index].src = subresult.picture[0].base64; 
                                }
                            }
                        ); 
                        
                        if( barcode !== ''){
                            if( barcode === wo_asset[i].barcode ){
                                arrAssets.push(sub_wo_asset[j]);
                            }
                        }else if (term !== ''){
                            //console.log( term );
                            var code_reference = typeof(sub_wo_asset[j].code_reference) !== 'undefined'?sub_wo_asset[j].code_reference.toLowerCase():sub_wo_asset[j].code_reference;
                            var asset_name = typeof(sub_wo_asset[j].asset_name) !== 'undefined'?sub_wo_asset[j].asset_name.toLowerCase():sub_wo_asset[j].asset_name;
                            var category_name = typeof(sub_wo_asset[j].category_name) !== 'undefined'?sub_wo_asset[j].category_name.toLowerCase():sub_wo_asset[j].category_name;
                            var location_name = typeof(sub_wo_asset[j].location_name) !== 'undefined'?sub_wo_asset[j].location_name.toLowerCase():sub_wo_asset[j].location_name;
                            var team_name = typeof(sub_wo_asset[j].team_name) !== 'undefined'?sub_wo_asset[j].team_name.toLowerCase():sub_wo_asset[j].team_name;
                            //console.log( code_reference );
                            if( 
                                code_reference.indexOf(term) > -1 ||
                                asset_name.indexOf(term) > -1 ||
                                category_name.indexOf(term) > -1 ||
                                location_name.indexOf(term) > -1 ||
                                team_name.indexOf(term) > -1
                            ){
                                arrAssets.push(sub_wo_asset[j]);
                            }
                        }else{           
                            arrAssets.push(sub_wo_asset[j]);
                        }
                    }
                    
                    //console.log( arrAssets );       
                    $scope.data = [{'type':'All', 'assets': arrAssets}];
                    $scope.initializeMap( arrAssets );
                }else if( type === 'category' ){
                    //get available category
                    var arrName     = [];
                    var currentName = '';   
                    for(var i=0;i<wo_asset.length;i++){
                        if( wo_asset[i].category_name  !== currentName ){
                            var arrAssets = [];
                            for(var j=0;j<sub_wo_asset.length;j++){
                                sub_wo_asset[j].src='img/ionic.png';
                                assetImagesModel.getbyassetwithindex( sub_wo_asset[j].asset_id, j ).then(
                                    function( subresult ){
                                        if( subresult.picture.length === 1 ) {
                                            sub_wo_asset[subresult.index].src = subresult.picture[0].base64; 
                                        }
                                    }
                                ); 
                                
                                if( barcode !== ''){
                                    if( barcode === wo_asset[i].barcode ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }else if (term !== ''){
                                    var code_reference = typeof(sub_wo_asset[j].code_reference) !== 'undefined'?sub_wo_asset[j].code_reference.toLowerCase():sub_wo_asset[j].code_reference;
                                    var asset_name = typeof(sub_wo_asset[j].asset_name) !== 'undefined'?sub_wo_asset[j].asset_name.toLowerCase():sub_wo_asset[j].asset_name;
                                    var category_name = typeof(sub_wo_asset[j].category_name) !== 'undefined'?sub_wo_asset[j].category_name.toLowerCase():sub_wo_asset[j].category_name;
                                    var location_name = typeof(sub_wo_asset[j].location_name) !== 'undefined'?sub_wo_asset[j].location_name.toLowerCase():sub_wo_asset[j].location_name;
                                    var team_name = typeof(sub_wo_asset[j].team_name) !== 'undefined'?sub_wo_asset[j].team_name.toLowerCase():sub_wo_asset[j].team_name;
                                    if( 
                                        code_reference.indexOf(term) > -1 ||
                                        asset_name.indexOf(term) > -1 ||
                                        category_name.indexOf(term) > -1 ||
                                        location_name.indexOf(term) > -1 ||
                                        team_name.indexOf(term) > -1
                                    ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }else{
                                    if( sub_wo_asset[j].category_name === wo_asset[i].category_name ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }
                            }
                            
                            arrName.push( { type:wo_asset[i].category_name, assets: arrAssets } );
                            currentName = wo_asset[i].category_name;
                        }
                    }
                    
                    $scope.data = arrName;
                }else if( type === 'location' ){
                    //get available location
                    var arrName     = [];
                    var currentName = '';   
                    for(var i=0;i<wo_asset.length;i++){
                        if( wo_asset[i].location_name  !== currentName ){
                            
                            var arrAssets = [];
                            for(var j=0;j<sub_wo_asset.length;j++){
                                sub_wo_asset[j].src='img/ionic.png';
                                assetImagesModel.getbyassetwithindex( sub_wo_asset[j].asset_id, j ).then(
                                    function( subresult ){
                                        if( subresult.picture.length === 1 ) {
                                            sub_wo_asset[subresult.index].src = subresult.picture[0].base64; 
                                        }
                                    }
                                ); 
                                
                                if( barcode !== ''){
                                    if( barcode === wo_asset[i].barcode ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }else if (term !== ''){
                                    var code_reference = typeof(sub_wo_asset[j].code_reference) !== 'undefined'?sub_wo_asset[j].code_reference.toLowerCase():sub_wo_asset[j].code_reference;
                                    var asset_name = typeof(sub_wo_asset[j].asset_name) !== 'undefined'?sub_wo_asset[j].asset_name.toLowerCase():sub_wo_asset[j].asset_name;
                                    var category_name = typeof(sub_wo_asset[j].category_name) !== 'undefined'?sub_wo_asset[j].category_name.toLowerCase():sub_wo_asset[j].category_name;
                                    var location_name = typeof(sub_wo_asset[j].location_name) !== 'undefined'?sub_wo_asset[j].location_name.toLowerCase():sub_wo_asset[j].location_name;
                                    var team_name = typeof(sub_wo_asset[j].team_name) !== 'undefined'?sub_wo_asset[j].team_name.toLowerCase():sub_wo_asset[j].team_name;
                                    if( 
                                        code_reference.indexOf(term) > -1 ||
                                        asset_name.indexOf(term) > -1 ||
                                        category_name.indexOf(term) > -1 ||
                                        location_name.indexOf(term) > -1 ||
                                        team_name.indexOf(term) > -1
                                    ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }else{
                                    if( sub_wo_asset[j].location_name === wo_asset[i].location_name ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }
                            }
                            
                            arrName.push( { type:wo_asset[i].location_name, assets: arrAssets } );
                            currentName = wo_asset[i].location_name;
                        }
                    }
                    
                    $scope.data = arrName;
                }else if( type === 'team' ){
                    //get available team
                    var arrName     = [];
                    var currentName = '';   
                    for(var i=0;i<wo_asset.length;i++){
                        if( wo_asset[i].team_name  !== currentName ){
                            
                            var arrAssets = [];
                            for(var j=0;j<sub_wo_asset.length;j++){
                                sub_wo_asset[j].src='img/ionic.png';
                                assetImagesModel.getbyassetwithindex( sub_wo_asset[j].asset_id, j ).then(
                                    function( subresult ){
                                        if( subresult.picture.length === 1 ) {
                                            sub_wo_asset[subresult.index].src = subresult.picture[0].base64; 
                                        }
                                    }
                                ); 
                                
                                if( barcode !== ''){
                                    if( barcode === wo_asset[i].barcode ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }else if (term !== ''){
                                    var code_reference = typeof(sub_wo_asset[j].code_reference) !== 'undefined'?sub_wo_asset[j].code_reference.toLowerCase():sub_wo_asset[j].code_reference;
                                    var asset_name = typeof(sub_wo_asset[j].asset_name) !== 'undefined'?sub_wo_asset[j].asset_name.toLowerCase():sub_wo_asset[j].asset_name;
                                    var category_name = typeof(sub_wo_asset[j].category_name) !== 'undefined'?sub_wo_asset[j].category_name.toLowerCase():sub_wo_asset[j].category_name;
                                    var location_name = typeof(sub_wo_asset[j].location_name) !== 'undefined'?sub_wo_asset[j].location_name.toLowerCase():sub_wo_asset[j].location_name;
                                    var team_name = typeof(sub_wo_asset[j].team_name) !== 'undefined'?sub_wo_asset[j].team_name.toLowerCase():sub_wo_asset[j].team_name;
                                    if( 
                                        code_reference.indexOf(term) > -1 ||
                                        asset_name.indexOf(term) > -1 ||
                                        category_name.indexOf(term) > -1 ||
                                        location_name.indexOf(term) > -1 ||
                                        team_name.indexOf(term) > -1
                                    ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }else{
                                    if( sub_wo_asset[j].team_name === wo_asset[i].team_name ){
                                        arrAssets.push(sub_wo_asset[j]);
                                    }
                                }
                            }
                            
                            arrName.push( { type:wo_asset[i].team_name, assets: arrAssets } );
                            currentName = wo_asset[i].team_name;
                        }
                    }
                    
                    $scope.data = arrName;
                }
                
                /* syncronize  survey */
                //check connection */
                if( typeof($scope.data) !== 'undefined' ){
                    var token = amtissData.getToken();
                    //console.log( token );
                    if( typeof( $scope.data[0].assets ) !== 'undefined' ){
                        if( $scope.data[0].assets.length > 0 ){
                            for(var i=0;i<$scope.data[0].assets.length;i++){
                                //console.log( $scope.data[0].assets[i].asset_id );
                                $scope.$parent.syncSurvey( token, $scope.data[0].assets[i].asset_id );
                            }
                        }
                    }
                }
                //console.log( $scope.data );
                //$scope.$parent.syncSurvey( $scope.token, $scope.assetId );
            }
        });
        
    }
    
    $scope.getListByFilter = function( type, term , barcode, name){
        
        var assetsByFilter = [];
        var uid = amtissData.getUid();
        //if( type === 'category' ){
            var qq = "SELECT *, '' as src FROM assets WHERE ( uid = ? ) AND ( " + type + "_name = ? ) ORDER BY server_id DESC";//, " + type + "_name ";
            var where = [uid, name];
                            
            if( term !== ''){
                qq = "SELECT *, '' as src FROM assets WHERE ( uid = ? ) AND ( " + type + "_name = ? ) " 
                    + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) )  ORDER BY server_id DESC "
                    + " ORDER BY server_id ";// + type + "_name ";
                where = [uid, name, '%' + term + '%', '%' + term + '%', '%' + term + '%' , '%' + term + '%' , '%' + term + '%' , '%' + term + '%' , '%' + term + '%' ];
            } 
        
            if( barcode !== '' ){
                var qq = "SELECT *, '' as src FROM assets WHERE ( uid = ? ) AND ( barcode = ? ) AND ( " + type + "_name = ? ) ORDER server_id DESC";// BY " + type + "_name ";
                where = [uid, barcode, name];
            }

            $cordovaSQLite.execute(db, qq, where).then(function(subresult) {
                if(subresult.rows.length > 0) {
                    
                    for(var i=0;i<subresult.rows.length;i++){
                        //var asset = subresult.rows.item(i);
                        
                        var asset = subresult.rows.item(i);
                        assetsByFilter.push( asset );
                        attributeModel.getbyassetwithindex( asset.server_id, i ).then(
                            function( attr ){
                                assetsByFilter[attr.index].attributes = attr.list; 
                            }
                        );
                        
                        //assetsByFilter.push(asset);
                        assetsByFilter[i].src = 'img/ionic.png';
                        
                        assetImagesModel.getbyassetwithindex( asset.server_id, i ).then(
                            function( subresult ){
                                if( subresult.picture.length === 1 ) {
                                    assetsByFilter[subresult.index].src = subresult.picture[0].base64; 
                                }
                            }
                        ); 
                    }
                    //console.log( JSON.stringify( $scope.data ) );
                    $scope.initializeMap( assetsByFilter );
                } else {
                    console.log("NO ROWS EXIST");
                }
            }, function(error) {
                console.error(error.message);
            });
        
        return assetsByFilter;
    };
    
    
    $scope.showList = function( type, term, barcode ){  
        //console.log( type + ' ' + term + ' ' + barcode );
        if( $rootScope.actionType > 0 ){
            $scope.showListByWO( type, term, barcode );
            return;
        }
        
        $scope.data.length      = 0;
        $scope.markers.length   = 0;
        
        $scope.type = type;
        
        if( barcode !== ''){
            term = '';
            $scope.assetterm = term;
        }
        
        var uid = amtissData.getUid();
        if( type === 'all'){    
            var q = "SELECT *, '' as src " +
                    " FROM assets "+
                    " WHERE uid = ? " +
                    " GROUP BY server_id ORDER BY server_id DESC";
            var where = [uid];
                
            if( term !== '' ){
                q       = " SELECT *, '' as src FROM assets WHERE (uid = ?)  AND "
                        + " ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) )  ORDER BY server_id DESC";
                where   = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
            
            if( barcode !== '' ){
                var q = "SELECT *, '' as src " +
                    " FROM assets "+
                    " WHERE uid = ?  AND barcode = ? " +
                    " GROUP BY server_id  ORDER BY server_id DESC";
                where = [uid, barcode];
            }
            
                      
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                assets.length = 0;
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length;i++){
                        var asset = result.rows.item(i);
                        assets.push( asset );
                        
                        attributeModel.getbyassetwithindex( asset.server_id, i ).then(
                            function( subresult ){
                                assets[subresult.index].attributes = subresult.list; 
                            }
                        );  
                        
                        assets[i].src = 'img/ionic.png';
                        
                        assetImagesModel.getbyassetwithindex( asset.server_id, i ).then(
                            function( subresult ){
                                if( subresult.picture.length === 1 ) {
                                    assets[subresult.index].src = subresult.picture[0].base64; 
                                }
                            }
                        );  
                    }
                                    
                    $scope.initializeMap( assets );
                    
//                    angular.forEach(result.rows,function(value,index){
//                        //alert(value.server_id);
//                        attributeModel.getbyasset( value.server_id ).then(
//                            function( subresult ){
//                                value.attributes = subresult;
//                                assets.push( value );
//                            }
//                        );
//                    });
                } else {
                    var alertPopUp = $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: amtissData.getMessage( 'no_asset_found' )
                         });
                    return;
                }
                    
                $scope.data = [{'type':'All', 'assets': assets}];
                
                //console.log( JSON.stringify( $scope.data ) );
                
            }, function(error) {
                console.error(error.message);
            });
        }else if( type === 'category' ){
            //console.log (' Type Category ');
            assetsAndType.length  = 0;
            
            //get available category sort by name
            var q = "SELECT DISTINCT category_name FROM assets WHERE ( uid = ? ) AND ( category_name IS NOT NULL ) ORDER BY category_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT category_name FROM assets WHERE ( uid = ? ) AND ( category_name IS NOT NULL ) " 
                    + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                    + " ORDER BY category_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
            if( barcode !== '' ){
                var q = "SELECT DISTINCT category_name FROM assets WHERE ( uid = ? ) AND ( barcode = ? ) AND ( category_name IS NOT NULL ) ORDER BY category_name ";
                where = [uid, barcode];
            }
            
                            
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).category_name;           
                        assetsAndType.push({'type':name, 'assets':$scope.getListByFilter( type, term , barcode, name) });
                            
                        $scope.data = assetsAndType;
                    }
                    
                    //console.log( JSON.stringify( $scope.data ) );
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_asset_found' )
                    });
                    return;
                }
                
            }, function(error) {
                console.error(error);
            });
        }else if( type === 'location' ){
            
            assetsAndType.length    = 0;
            
            //get available location sort by name
            var q = "SELECT DISTINCT location_name FROM assets WHERE ( uid = ? ) AND ( location_name IS NOT NULL ) ORDER BY location_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT location_name FROM assets WHERE ( uid = ? ) AND ( location_name IS NOT NULL ) " 
                    + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                    + " ORDER BY location_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
            
            if( barcode !== '' ){
                var q = "SELECT DISTINCT location_name FROM assets WHERE ( uid = ? ) AND ( barcode = ? ) AND ( location_name IS NOT NULL ) ORDER BY location_name ";
                where = [uid, barcode];
            }
                
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).location_name;           
                        assetsAndType.push({'type':name, 'assets':$scope.getListByFilter( type, term , barcode, name) });
                            
                        $scope.data = assetsAndType;
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_asset_found' )
                    });
                    return;
                }
            }, function(error) {
                console.error(error);
            });
        }else if( type === 'team' ){
            
            assetsAndType.length    = 0;
            
            //if( $scope.$parent.isMobile() === true ) {
                //get available team sort by name
                var q = "SELECT DISTINCT team_name FROM assets WHERE ( uid = ? ) AND ( team_name IS NOT NULL ) ORDER BY team_name ";
                var where = [uid];
                
                if( term !== ''){
                    q = "SELECT DISTINCT team_name FROM assets WHERE ( uid = ? ) AND ( team_name IS NOT NULL ) " 
                        + " AND ( ( code_reference LIKE ? ) OR ( asset_name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) OR ( description LIKE ? ) OR ( address LIKE ? ) ) "
                        + " ORDER BY team_name ";
                    where = [uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
                }
            
            if( barcode !== '' ){
                var q = "SELECT DISTINCT team_name FROM assets WHERE ( uid = ? ) AND ( barcode = ? ) AND ( team_name IS NOT NULL ) ORDER BY team_name ";
                where = [uid, barcode];
            }
                
                $cordovaSQLite.execute(db, q, where).then(function(result) {
                    if(result.rows.length > 0) {
                        for(var i=0;i<result.rows.length; i++){
                            var name = result.rows.item(i).team_name;           
                            assetsAndType.push({'type':name, 'assets':$scope.getListByFilter( type, term , barcode, name) });
                            //console.log( 'Name....: ' + name );
                            
                            $scope.data = assetsAndType;
                        }
                    } else {
                        //console.log("NO ROWS EXIST");
                        var alertPopUp = $ionicPopup.alert({
                              title: amtissData.getAppTitle(),
                              template: amtissData.getMessage( 'no_asset_found' )
                         });
                         return;
                    }
                }, function(error) {
                    console.error(error);
                });
                
                
                
            //}else{
            //    $scope.data = [ { 'type':'Location 1', 'assets': assetsData.list.slice(0,1) }, { 'type':'Location 2', 'assets': assetsData.list.slice(1,4) }];
             //   $scope.markers.length = 0;
                
//                for(var i=0;i<assetsData.list.slice(0,3).length;i++){
//                //console.log (i + '   ' +  assetsData.list[i] );
//                    $scope.markers.push({
//                        id: assetsData.list[i].code_reference,
//                        latitude: assetsData.list[i].latitude,
//                        longitude: assetsData.list[i].longitude,
//                        title: assetsData.list[i].code_reference
//                    })
//                }
            
            //}
        }
    };
    
    $scope.scan = function(){
         if( typeof cordova === 'undefined' ){
             return; 
         }
        
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner
            .scan()
            .then(function(imageData) {
                // Success! Barcode data is here
                //console.log( text );
//                alert(imageData.text);
//                alert("Barcode Format -> " + imageData.format);
//                alert("Cancelled -> " + imageData.cancelled);
                if( !imageData.calleled ){
                    $scope.showList( $scope.type, '', imageData.text);
                }
                
            }, function(error) {
                // An error occurred
                console.log( error );
                alert( 'error ' + error );
            });
        });
    };
        
//    $scope.getAssetByAttribute = function( asset_id ){
//        var q = "SELECT * FROM attributes WHERE asset_id = (?) ORDER BY asset_attributes_name ";
//        var where = [asset_id];
//        var arrAttr = [];
//        $cordovaSQLite.execute(db, q, where).then(function(result) {
//            if( result.rows.length > 0 ){
//                for(var i=0;i<result.rows.length; i++){
//                    artAttr.push(result.rows.item(i));
//                }
//            }
//            return arrAttr;
//        }, function(error) {
//            console.error(error);
//            return arrAttr;
//        });
//        
//        
//    }
    
    $scope.initializeMap = function(assetsmap){
        if( assetsmap.length > 0 ){
            if( typeof $scope.map === 'undefined'){
                $scope.centerLat = assetsmap[0].latitude;
                $scope.centerLong = assetsmap[0].longitude;
                $scope.map = {center: {latitude: $scope.centerLat, longitude:$scope.centerLong }, zoom: 16 };
                $scope.options = {scrollwheel: false};
            }
            
            //console.log( assetsmap );
       
            for(var i=0;i<assetsmap.length;i++){
                
                if( assetsmap[i].latitude != '' && assetsmap[i].latitude != '' ){
                    var mar = {
                                id: assetsmap[i].code_reference,
                                latitude: assetsmap[i].latitude,
                                longitude: assetsmap[i].longitude,
                                title: assetsmap[i].asset_name
                            };

                    mar.onClick = function() {
                        console.log("Clicked!");
                        //mar.show = !mar.show;
                    };
                    

                    $scope.markers.push( mar );
                }
            }

            $scope.map.center.latitude = assetsmap[0].latitude;
            $scope.map.center.longitude = assetsmap[0].longitude;
        }
    };
})

.controller('AssetDetailsCtrl', function($scope, $stateParams, $cordovaBarcodeScanner, 
    $ionicPlatform, $ionicPopup, amtissData, attributeModel, assetModel,
    assetSurveyModel, assetImagesModel, $ionicPopover) {
        
    $scope.uid = null;
    $scope.asset_ids = [];
    $scope.asset_gallery = [];
    $scope.asset_gallery_length = 0;
    $scope.asset_gellery_index = 0;
    
    //Context menu
    $ionicPopover.fromTemplateUrl('templates/assetActions.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
        popover.hide();
    });
    
    $scope.init = function() {
        $scope.uid = amtissData.getUid();
        $scope.assetId = $stateParams.assetId;
        $scope.startingOfflineId = amtissData.getStartingOfflineId();
        
        assetSurveyModel.setReferrer('init');
        
        assetModel.getserverids( $scope.uid ).then(
            function( result ){
                $scope.asset_ids = result;
                $scope.asset_ids.unshift(-1);
                $scope.asset_ids.push(-2);
                //console.log( $scope.asset_ids ); 
                $scope.showDetails( $scope.assetId );
            }
        );
    };
    
    $scope.showDetails = function( id ){
        assetModel.getbyid( id, $scope.uid ).then(
            function(result){
                $scope.asset = result;
                $scope.setPrevNext( id );
            }
        );

        attributeModel.getbyasset( $scope.assetId).then(
            function(result){
                $scope.attributes = result;
            }
        );
        
        assetImagesModel.byasset( id ).then(
            function(result){
                $scope.asset_gallery = result;
                
                $scope.asset_gallery_length = result.length;
                $scope.asset_gellery_index = 0;
            }
        );
    }
    
    $scope.prevGalleryPicture = function(){
        $scope.asset_gellery_index -= 1;
    }
    
    $scope.nextGalleryPicture = function(){
        $scope.asset_gellery_index += 1;
    }
    
    $scope.setGelleryIndex = function( index ){
        $scope.asset_gellery_index = index;
    }
    
    $scope.setPrevNext = function( id ){
        
        var length      = $scope.asset_ids.length;
        var index       = 0;
        //var indexMax    = length - 1;
        $scope.prev_id = 0;
        $scope.next_id = 0;

        if( length > 0 ){
            for(var i=0;i<length;i++){
                
                if( Number($scope.asset_ids[i]) == Number(id) ){
                    //console.log( 'ids : ' + $scope.asset_ids[i]);
                    $scope.prev_id = $scope.asset_ids[i-1];
                    $scope.next_id = $scope.asset_ids[i+1];
                    break;
                }
            } 
            
            
            //console.log( 'prev ' + $scope.prev_id );
            //console.log( 'next ' + $scope.next_id );
        }
    }
    
    $scope.scan = function(){
        
        if( typeof cordova === 'undefined' ){
             return; 
         }
        
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner
            .scan()
            .then(function(imageData) {
//                alert(imageData.text);
//                alert("Barcode Format -> " + imageData.format);
//                alert("Cancelled -> " + imageData.cancelled);
                if( !imageData.calleled ){
                    assetModel.getbybarcode( imageData.text, amtissData.getUid() ).then(
                        function( result ){
                            //console.log('Edit: Asset ID: ' + result );
                            if( result > 0 ){
                                $scope.showDetails( result );
                            }else{
                                var alertPopUp = $ionicPopup.alert({
                                      title: amtissData.getAppTitle(),
                                      template: amtissData.getMessage( 'no_asset_found' )
                                });
                                 return;
                            }
                        }
                    );
                }
                
            }, function(error) {
                // An error occurred
                console.log( error );
                alert( 'error ' + error );
            });
        });
    }
    
    
})

.controller('AssetAddCtrl', function($scope, $stateParams, 
                                        $cordovaBarcodeScanner, 
                                        $ionicPlatform,
                                        $ionicPopup,
                                        $timeout, 
                                        $ionicHistory,
                                        $cordovaGeolocation,
                                        $q, $state, 
                                        $cordovaToast,
                                        $ionicLoading,
                                        amtissData,
                                        assetModel,
                                        assetImagesModel,
                                        locationModel,
                                        categoryModel,
                                        companyModel,
                                        vendorModel,
                                        teamModel,
                                        attributeModel,
                                        attModel,
                                        assetSurveyModel,
                                        amtissService) {
    $scope.uid = null;
    
    $scope.data = {};
    $scope.assets = [];
    $scope.locations = [];
    $scope.categories = [];
    $scope.companies = [];
    $scope.vendors = [];
    $scope.teams = [];
    $scope.attributes = [];
    $scope.atts = [];
    $scope.markers = [];
    $scope.data.latitude = "-0.513658";
    $scope.data.longitude = "117.093711";
    
    $scope.asset_gallery = [];
    $scope.asset_gallery_length = 0;
    $scope.asset_gellery_index = 0;
    $scope.asset_ids = [];
    
    $scope.data.attributes = [{server_id:0, asset_id:0, attribute_value_asset:0, asset_attributes_name:'', 
                               attribute_value_name:'', action:'C'}];
    
    $scope.init = function() {    
        $scope.assets.length = 0;
        $scope.startingOfflineId = amtissData.getStartingOfflineId();
        
        assetSurveyModel.setReferrer('init');
        
        assetModel.autocomplete( amtissData.getUid() ).then(function(result) {
            $scope.assets = result;
        });
        
        $scope.locations.length = 0;
        locationModel.autocomplete().then(function(result) {
            $scope.locations = result;
        });
        
        categoryModel.autocomplete().then(function(result) {
            $scope.categories = result;
        });
        
        $scope.companies.length = 0;
        companyModel.autocomplete().then(function(result) {
            $scope.companies = result;
        });
        
        $scope.vendors.length = 0;
        vendorModel.autocomplete().then(function(result) {
            $scope.vendors = result;
        });
        
        $scope.teams.length = 0;
        teamModel.autocomplete().then(function(result) {
            $scope.teams = result;
        });
        
        $scope.atts.length = 0;
        attModel.autocomplete().then(function(result) {
            $scope.atts = result;
        });
        
        $scope.action = $stateParams.assetId > 0 ?'Edit':'Add';
        $scope.data.token = amtissData.getToken();
        $scope.data.isajax = 1;
        $scope.data.action = 'C';
        
        assetModel.getserverids( amtissData.getUid() ).then(
            function( result ){
                $scope.asset_ids = result;
                $scope.asset_ids.unshift(-1);
                $scope.asset_ids.push(-2);
                $scope.showAssetForm( $stateParams.assetId );
            }
        );
    };
    
    $scope.setLatitudeLongitude = function(){
        
        var posOptions = {timeout: 10000, enableHighAccuracy: true};
        $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
            //console.log( position );
            //var lat  = position.coords.latitude
            //var long = position.coords.longitude
            $scope.data.latitude = position.coords.latitude;
            $scope.data.longitude = position.coords.longitude;
            $scope.title = 'Latitude: ' + $scope.data.latitude + '  Longitude: ' + $scope.data.longitude;
        }, function(err) {
            $scope.data.latitude = 0;
            $scope.data.longitude = 0;
          // error
            var alertPopUp = $ionicPopup.alert({
                                                title: amtissData.getAppTitle(),
                                                template: 'Geotangging: '+ err.message
                                                });
                                     //return;
            $scope.title = 'Latitude: ' + $scope.data.latitude + '  Longitude: ' + $scope.data.longitude;
        });
    };
    
    $scope.showAssetForm = function( asset_id ){
        $scope.data.asset_id = asset_id;
        $scope.data.uid = amtissData.getUid();
        $scope.data.token = amtissData.getToken();
        $scope.prev_id = -1;
        $scope.next_id = -2;
        $scope.data.code_reference = '';
        $scope.data.asset_name = '';
        $scope.data.barcode = '';
        $scope.data.address = '';
        $scope.data.description = '';
        
        if( $scope.data.asset_id > 0 ){
            assetModel.getbyid( $scope.data.asset_id, $scope.data.uid ).then(
                function(result){
                    $scope.setPrevNext( $scope.data.asset_id );
                    $scope.data = result;
                    $scope.data.action = 'U';
                    $scope.data.isajax = 1;
                    $scope.data.token = amtissData.getToken();
                    $scope.title = 'Latitude: ' + $scope.data.latitude + '  Longitude: ' + $scope.data.longitude;
                }
            );

            $scope.data.attributes.length = 0;
            attributeModel.getbyasset( $scope.data.asset_id ).then(
                function(result){
                    $scope.data.attributes = result;
                    //console.log( result );
                    if( $scope.data.attributes.length > 0 ){
                        for( var i=0;i< $scope.data.attributes.length;i++ ){
                            if( $scope.data.attributes[i].action !== 'D' ){
                                $scope.data.attributes[i].action = 'U';
                            }
                        }
                        
//                        angular.forEach($scope.data.attributes, function(value, key) { 
//                            console.log( value.asset_attributes_name );
//                            console.log( value.attribute_value_name );
//                        });
                    }
                }
            );
            
            assetImagesModel.byasset(  $scope.data.asset_id ).then(
                function(result){
                    $scope.asset_gallery_length = result.length;
                    $scope.asset_gallery = result;
                    $scope.asset_gellery_index = 0;
                }
            );
        }else{
            /* geolocation */
            $scope.setLatitudeLongitude();
        }
    }
    
    $scope.prevGalleryPicture = function(){
        $scope.asset_gellery_index -= 1;
    }
    
    $scope.nextGalleryPicture = function(){
        $scope.asset_gellery_index += 1;
    }
    
    $scope.setGelleryIndex = function( index ){
        $scope.asset_gellery_index = index;
    }
    
    $scope.setPrevNext = function( id ){
        var length      = $scope.asset_ids.length;
        var index       = 0;
        $scope.prev_id = -1;
        $scope.next_id = -2;

        if( length > 0 ){
            for(var i=0;i<length;i++){
                
                if( Number($scope.asset_ids[i]) == Number(id) ){
                    //console.log( 'ids : ' + $scope.asset_ids[i]);
                    $scope.prev_id = $scope.asset_ids[i-1];
                    $scope.next_id = $scope.asset_ids[i+1];
                    break;
                }
            } 
            
            
            //console.log( 'prev ' + $scope.prev_id );
            //console.log( 'next ' + $scope.next_id );
        }
    }
    
    $scope.scan = function(){
        
        if( typeof cordova === 'undefined' ){
             return; 
         }
        
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner
            .scan()
            .then(function(imageData) {
                if( !imageData.calleled ){
                    if( $scope.action === 'Edit' ){
                        assetModel.getbybarcode( imageData.text, amtissData.getUid() ).then(
                            function( result ){
                                //console.log('Edit: Asset ID: ' + result );
                                if( result > 0 ){
                                    $scope.showAssetForm( result );
                                }else{
                                    var alertPopUp = $ionicPopup.alert({
                                          title: amtissData.getAppTitle(),
                                          template: amtissData.getMessage( 'no_asset_found' )
                                     });
                                     return;
                                }
                            }
                        );
                    }else{
                        //console.log('Add: Barcode: ' + imageData.text );
                        $scope.data.barcode = imageData.text;
                    }
                }
                
            }, function(error) {
                // An error occurred
                console.log( error );
                //alert( 'error ' + error );
            });
        });
    }
    
    $scope.getAssetItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.assets.length; i++ ){
                var name = $scope.assets[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    items.push($scope.assets[i]);
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
    
    $scope.itemAssetClicked = function (callback) {
        $scope.data.parent_id = callback.item.id;
        $scope.data.parent_name = callback.item.name;
    }
    
    $scope.getLocationItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.locations.length; i++ ){
                var name = $scope.locations[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    //if( item.toLowerCase().indexOf( name.toLowerCase() ) === -1 ){
                        items.push($scope.locations[i]);
                    //}
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
    
    $scope.itemLocationClicked = function (callback) {
        $scope.data.location_id = callback.item.id;
        $scope.data.location_name = callback.item.name;
    }
    
     $scope.getCategoryItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.categories.length; i++ ){
                var name = $scope.categories[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    items.push($scope.categories[i]);
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
    
    $scope.itemCategoryClicked = function (callback) {
        $scope.data.category_id = callback.item.id;
        $scope.data.category_name = callback.item.name;
    }
    
    $scope.getCompanyItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.companies.length; i++ ){
                var name = $scope.companies[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    items.push($scope.companies[i]);
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
    
    $scope.itemCompanyClicked = function (callback) {
        $scope.data.company_id = callback.item.id;
        $scope.data.company_name = callback.item.name;
    }
    
     $scope.getVendorItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.vendors.length; i++ ){
                var name = $scope.vendors[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    items.push($scope.vendors[i]);
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
    
    $scope.itemVendorClicked = function (callback) {
        $scope.data.vendor_id = callback.item.id;
        $scope.data.vendor_name = callback.item.name;
    }
    
     $scope.getTeamItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.teams.length; i++ ){
                var name = $scope.teams[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    items.push($scope.teams[i]);
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
     
     
    $scope.itemTeamClicked = function (callback) {
        $scope.data.team_id = callback.item.id;
        $scope.data.team_name = callback.item.name;
    }
    
    $scope.getAttributeItems = function( query ){
        var returnValue = { items: [] };
        var items = [];
        
        if( query.length > 1 ){
            for( var i=0;i< $scope.atts.length; i++ ){
                var name = $scope.atts[i].name.toLowerCase();
                if(name.indexOf( query.toLowerCase() ) !== -1){
                    items.push($scope.atts[i]);
                }
            }
        }
            
        returnValue.items = items;
        return returnValue;
    }
    
    $scope.itemAttributeClicked = function (callback, index ) {
        //console.log( callback );
//        console.log( index );
//        $scope.data.attribute_id = callback.item.id;
//        $scope.data.attribute_name = callback.item.name;
        if( typeof callback.item !== 'undefined'){
            $scope.data.attributes[index].attribute_value_asset = callback.item.id;
        }
    }
    
    
    $scope.addNewAttribute = function(){
        var newAttribute = {server_id:0, asset_id:0, attribute_value_asset:0, asset_attributes_name:'', 
                                attribute_value_name:'', action:'C'};
        $scope.data.attributes.push( newAttribute );
    }
    
    $scope.removeAttribute = function(index){
        if( $scope.data.attributes.length > 1 ){
            $scope.data.attributes[index].action = 'D'
        }
        //console.log( $scope.data.attributes );
    }
    
    $scope.save = function(){
        //console.log( $scope.data.asset_name.trim().length );
        //validation
        if( $scope.data.asset_name.trim() === '' ){
            $ionicPopup.alert({
                title: amtissData.getAppTitle(),
                template: 'The Asset Name is required.'
            }); 
            return;
        }
            
//        if( $scope.data.code_reference.trim() === '' ){
//            $ionicPopup.alert({
//                title: amtissData.getAppTitle(),
//                template: 'The Code Reference is required.'
//            }); 
//            return;
//        }
            
        $timeout(function(){
            $ionicLoading.show({});
        }, 200)
            

        if( $scope.$parent.isOnline() !== true ){
            $ionicPopup.alert({
                title: amtissData.getAppTitle(),
                template: amtissData.getMessage('offline_mode')
            }); 
            
//            if( $scope.$parent.isMobile() ){
//                $cordovaToast.showLongCenter(amtissData.getMessage('offline_mode'));
//            }
                        
            //server_id dari asset yang di simpan offline saya tetapkan dimulai dari 10juta
            //gunanya supaya ada foreignkey untuk attribute dan picture yang di buat waktu 
            //offline
            /*
                1. check offline_id or server_id
                2. offline_id       -> action == 'C', server_id = offline_id
                3. not offline_id   -> action == 'U', server_id = server_id
            */
            $scope.startingOfflineId = amtissData.getStartingOfflineId();
            //console.log( $scope.startingOfflineId );
            var server_id = $scope.startingOfflineId;
            if( typeof( $scope.data.id ) !== 'undefined' ){
                server_id = $scope.data.server_id;
            }
            
            var action    = ( server_id < $scope.startingOfflineId )? 'U':'C';
            var survey_id = (action === 'C')?0:$scope.data.survey_id;
            var value = $scope.data;
            
            var assetparameters = {
                                    server_id: server_id,
                                    uid: value.uid,
                                    code_reference: value.code_reference,
                                    asset_name: value.asset_name,
                                    location_id: typeof(value.location_id) == 'undefined' ? '' : value.location_id,
                                    location_name: typeof(value.location_name) == 'undefined' ? '' : value.location_name,
                                    team_id: typeof(value.team_id) == 'undefined' ? '' : value.team_id,
                                    team_name: typeof(value.team_name) == 'undefined' ? '' : value.team_name,
                                    category_id: typeof(value.category_id) == 'undefined' ? '' : value.category_id,
                                    category_name: typeof(value.category_name) == 'undefined' ? '' : value.category_name,
                                    parent_id: typeof(value.parent_id) == 'undefined' ? '' : value.parent_id, 
                                    parent_name: typeof(value.parent_name) == 'undefined' ? '' : value.parent_name,
                                    company_id: typeof(value.company_id) == 'undefined' ? '' : value.company_id,
                                    company_name: typeof(value.company_name) == 'undefined' ? '' : value.company_name,
                                    latitude: typeof(value.latitude) == 'undefined' ? '' : value.latitude,
                                    longitude: typeof(value.longitude) == 'undefined' ? '' : value.longitude,
                                    barcode: typeof(value.barcode) == 'undefined' ? '' : value.barcode,
                                    description: typeof(value.description) == 'undefined' ? '' : value.description,
                                    address: typeof(value.address) == 'undefined' ? '' : value.address,
                                    create_date: typeof(value.create_date) == 'undefined' ? '' : value.create_date,
                                    action: action,
                                    survey_id: survey_id
                                };
            //console.log( assetparameters ); return;
//            console.log( $scope.data );

            if( typeof($scope.data.id) !== 'undefined' ){//edit
                
                assetModel.addOrUpdateAsset( assetparameters);
                console.log( 'edit' );
                
                /* ATTRIBUTES */
                /* set as deleted */
                //attributeModel.flag ( assetparameters.server_id );
                
//
                //console.log( assetparameters.server_id );
                //console.log( $scope.data );return;
                //var date = new Date();
                //console.log( new Date().valueOf() );
                attributeModel.flag ( assetparameters.server_id ).then(function( result ){
                    attributeModel.createAttributesParameters( $scope.data.attributes, 
                                                              assetparameters.server_id, false );
                });
                $timeout(function(){
                    //$ionicHistory.goBack();
                    $ionicLoading.hide();
                    attributeModel.removebyflag(assetparameters.server_id);
                    $state.go('app.assets');
                }, 2000);
            }
            else{
                console.log( 'new ' );
                assetModel.serveroflineid( server_id ).then( function( serverlocalid ) {
                    assetparameters.id = serverlocalid + 1; 
                    
                    assetModel.addOrUpdateAsset( assetparameters);

                    /* ATTRIBUTES */
                    /* set as deleted */
                    attributeModel.flag ( assetparameters.server_id ).then(function( result ){
                        attributeModel.createAttributesParameters( $scope.data.attributes, 
                                                                  assetparameters.server_id, false );
                    });
                    $timeout(function(){
                        //$ionicHistory.goBack();
                        $ionicLoading.hide();
                        attributeModel.removebyflag(assetparameters.server_id);
                        $state.go('app.assets');
                    }, 2000);
                });
            }
        }else{
            
            var isDataOffline = false;
            //var serverIdOffline = 0;
            var attritbutesIdOffline = [];
            var clientId = $scope.data.server_id;
            var startingOfflineId = amtissData.getStartingOfflineId();
            //console.log( $scope.data.server_id);return;
            if( $scope.data.server_id >= startingOfflineId ){
                isDataOffline = true;
                //serverIdOffline = $scope.data.server_id;
                
                $scope.data.server_id = 0; //server id yang di buat offline diset kembali 0 untuk di kirim ke server
                $scope.data.action = 'C';  //actionnya juga di set jadi 'C'
            }
            
            //attribute juga diset actionnya jadi C
            if( typeof( $scope.data.attributes) !== 'undefined' ){
                if( $scope.data.attributes.length > 0 ){
                   //console.log( 'length : '+ $scope.data.attributes.length );
                    for( var j=0;j<$scope.data.attributes.length;j++ ){
                        //console.log ( $scope.data.attributes[j].server_id );
                        if( $scope.data.attributes[j].server_id > amtissData.getStartingOfflineId() ){
                            $scope.data.attributes[j].action = 'C';
                        }
                    }
                }
            }
            
           //console.log( $scope.data );
           //return;
            amtissService.doRequest( 'assets', $scope.data )
            .then(
                function( response ) {
                    //console.log( response );
                    if( response.status === true ){

                        var server_id = response.server_id;
                        //var client_id = response.client_id;
                        
                        if( isDataOffline ){
                            //console.log( response.status );
                            assetModel.updateOfflineId( server_id, clientId);
                        }

                        var value = $scope.data;
                        assetparameters = {
                                server_id: server_id,
                                uid: value.uid,
                                code_reference: value.code_reference,
                                asset_name: value.asset_name,
                                location_id: typeof(value.location_id) == 'undefined' ? '' : value.location_id,
                                location_name: typeof(value.location_name) == 'undefined' ? '' : value.location_name,
                                team_id: typeof(value.team_id) == 'undefined' ? '' : value.team_id,
                                team_name: typeof(value.team_name) == 'undefined' ? '' : value.team_name,
                                category_id: typeof(value.category_id) == 'undefined' ? '' : value.category_id,
                                category_name: typeof(value.category_name) == 'undefined' ? '' : value.category_name,
                                parent_id: typeof(value.parent_id) == 'undefined' ? '' : value.parent_id, 
                                parent_name: typeof(value.parent_name) == 'undefined' ? '' : value.parent_name,
                                company_id: typeof(value.company_id) == 'undefined' ? '' : value.company_id,
                                company_name: typeof(value.company_name) == 'undefined' ? '' : value.company_name,
                                latitude: typeof(value.latitude) == 'undefined' ? '' : value.latitude,
                                longitude: typeof(value.longitude) == 'undefined' ? '' : value.longitude,
                                barcode: typeof(value.barcode) == 'undefined' ? '' : value.barcode,
                                description: typeof(value.description) == 'undefined' ? '' : value.description,
                                address: typeof(value.address) == 'undefined' ? '' : value.address,
                                create_date: typeof(value.create_date) == 'undefined' ? '' : value.create_date,
                                survey_id: typeof(value.survey_id) == 'undefined' ? '' : value.survey_id,
                                action: 'O'
                            };

                        assetModel.addOrUpdateAsset( assetparameters).then(function( result ){
                            
                            /* ATTRIBUTES */
                            /* set as deleted */
//                            if( isDataOffline ){
//                                if( typeof(response.attributes) !== 'undefined' ){
//                                    if( response.attributes.length > 0 ){
//                                        for(var k=0;k<response.attributes.length;k++ ){
//                                            attributeModel.updateOfflineId( response.attributes[k].attribute_value_id, 
//                                                                           attritbutesIdOffline[k], server_id);
//                                        }
//                                    }
//                                }
//                            }
//                            else{
                            
//                            attributeModel.flag ( assetparameters.server_id ).then(function( result ){
//                            attributeModel.createAttributesParameters( response.attributes, 
//                                                                          assetparameters.server_id, false );
//                            });
//                           }
                            //sync attributes
                            $scope.syncAttributes (server_id );
                                                            
                            //sync pictures
                            $scope.syncPicture( server_id, clientId);
                            
                            
                            $timeout(function(){
                                $ionicLoading.hide();
                                //attributeModel.removebyflag(assetparameters.server_id);
                                $state.go('app.assets');
                            }, 2000);
                        });

                    }else{
                        $ionicPopup.alert({
                            title: amtissData.getAppTitle(),
                            template: response.message
                        });
                    }
                }
            );
        }
    }
    
    
    $scope.syncAttributes = function(asset_id){
        //get images by client id
        //console.log( asset_id );
        var uid = amtissData.getUid();
        console.log( $scope.data.attributes);
        
        var q = $q.defer();
        var promises = [];
        
        if( typeof( $scope.data.attributes) !== 'undefined' ){
            if( $scope.data.attributes !== null ){
                for(var i=0;i<$scope.data.attributes.length;i++){
                    //if( $scope.data.attributes[i].action === 'C' || $scope.data.attributes[i].action === 'U' ){
                        promises.push($scope.syncAttribute( asset_id, uid, $scope.data.attributes[i]));
//                    }else{
//                        $scope.deleteAttribute( asset_id, uid, $scope.data.attributes[i]);
                    //}
                }
            }
        }
                                      
        $q.all(promises).then(function() {
            $timeout(function(){
                //attributeModel.removebyflag( asset_id );
            }, 5000);
        });
    }
    
    $scope.syncAttribute = function( asset_id, uid, attribute ){
        console.log( new Date().valueOf() + ' save' );
    
        if( $scope.$parent.isOnline() === false ){
            $ionicPopup.alert({
                title: amtissData.getAppTitle(),
                template: amtissData.getMessage('no_internet_connection')
            }); 
            return;
        }
        
        //$timeout( function() {
        attribute.isajax            = 1;
        attribute.token     = amtissData.getToken();
        attribute.uid       = uid;
        attribute.asset_id  = asset_id;
        //attribute.server_id = attribute.server_id;
        console.log( attribute );
        
        amtissService.doRequest( 'attribute', attribute )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    if( typeof( response.server_id) !== 'undefined' ){
                        attribute.server_id = response.server_id;
                        attribute.action = 'O';
                    }
                    
                    var clientId = response['client_id'];
                    if( clientId > amtissData.getStartingOfflineId() ){
                        attributeModel.getundefinedname( Number(attribute.attribute_value_asset) ).then(function( result ){
                                var name = result.name;
                                
                                //console.log( 'update attrs' );
                                var parameters  = [ attribute.server_id, attribute.asset_id, name, 
                                                   attribute.attribute_value_name, attribute.attribute_value_asset, 'O', 
                                                   'N', clientId ];
                               
                               attributeModel.updateOfflineData( parameters ); 
                         });
                    }
                    else{
                        attributeModel.createAttributeParameters( attribute, asset_id, false );
                    }
                    
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
    }
    
    $scope.syncPicture = function(server_id, client_id){
        //get images by client id
        console.log( client_id );
        console.log( server_id );
        assetImagesModel.getallbyassetid( client_id ).then(function( result ){
            console.log( result );
            if( typeof(result) !== 'undefined' ){
                for( var i=0;i<result.length;i++ ){
                    console.log( result[i].action );
                    if( result[i].action === 'C') {
                        $scope.uploadPicture( result[i], server_id );
                    }else if( result[i].action === 'D') {
                        $scope.deletePicture( result[i], server_id );
                    }
                }
            }
        });
    }
    
    $scope.uploadPicture = function( picture, asset_id ){
        console.log( new Date().valueOf() + ' upload' );
        console.log( picture );
        
        if( $scope.$parent.isOnline() === false ){
            $ionicPopup.alert({
                title: amtissData.getAppTitle(),
                template: amtissData.getMessage('no_internet_connection')
            }); 
            return;
        }

        //return;
        //$timeout( function() {
        picture.isajax = 1;
        picture.token = amtissData.getToken();
        //picture.index = index;
        picture.asset_id = asset_id;

        amtissService.doRequest( 'assetimages', picture )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    assetImagesModel.updateserveridandassetid( response.server_id, response.client_id, asset_id);
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
    }
    
    $scope.deletePicture = function( picture, asset_id ){
        //console.log( picture );    
        var picture = {isajax:1, token: amtissData.getToken(), server_id:picture.server_id, action:'D' };
        amtissService.doRequest( 'assetimages', picture )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    assetImagesModel.removebyserverid( picture.server_id );
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
    }
})

.controller('AssetImagesCtrl', function($scope, $stateParams, $ionicPlatform,
                                         $timeout,
                                         $ionicPopup,
                                            $cordovaCamera, $q,
                                            amtissData,
                                            assetModel,
                                            assetImagesModel,
                                            amtissService){
    $scope.uid = 0;
    $scope.asset = {};
    $scope.src = '';
    $scope.newImage = {};
    $scope.mainImage
    $scope.mainContenxt;
    $scope.mainCanvas;
    $scope.canvasW = 960;
    $scope.canvasH = 600; 
    $scope.logo;
    $scope.showUploadButton = false;
    
    $scope.pictures = [];
    
    $scope.init = function(){
        $scope.assetId = $stateParams.assetId;
        $scope.uid = amtissData.getUid();
        $scope.startingOfflineId = amtissData.getStartingOfflineId();
        
        $scope.token = amtissData.getToken();
        //$scope.src = mobile_02;
        
        assetModel.getbyid( Number($scope.assetId), $scope.uid ).then(
            function( result ){
                $scope.asset = result;
                //$scope.showMainImage();
            }
        );
                
        $scope.getLocalPictures();
    }
    
    $scope.getLocalPictures = function(){
        //console.log( $scope.assetId );
        $scope.pictures.length = 0;
        assetImagesModel.getbyassetid( $scope.assetId ).then(
            function( result ){
//                console.log( result );
                $scope.pictures = result;
                $scope.checkShowButton();
            }
        );   
    }
    
    $scope.checkShowButton = function(){
        $scope.showUploadButton = false;
        if( $scope.pictures.length > 0 ){
            for( var i=0;i<$scope.pictures.length; i++ ){
                if( $scope.pictures[i].action == 'C'){
                    $scope.showUploadButton = true;
                }
            }
        }
    }
    
    $scope.getCanvasHeight = function( w ){
        return (0.625 * w)
    }
    
    $scope.showMainImage = function(){
        
        $scope.logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAAGWB6gOAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjUxMDY0MTFFNEE5QzExRTVCRTlCQzYxMEQ1QTc4NjM3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjUxMDY0MTFGNEE5QzExRTVCRTlCQzYxMEQ1QTc4NjM3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTEwNjQxMUM0QTlDMTFFNUJFOUJDNjEwRDVBNzg2MzciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTEwNjQxMUQ0QTlDMTFFNUJFOUJDNjEwRDVBNzg2MzciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5GFsoyAAAHPUlEQVR42mL8//8/AzpgARGT9tr///2PkSF2/1OE4MY2FgZ5I3ZGBlYGhr9svJ/BgsvNH4FVrHCW+v/7B+NCJqgxjEAMMpyRleP/H4AAYkS3iGXyQXuG/38Y/kfsfQYR2NDEAlYeYc7AEH5c5j/Ltx8/GDbav/oPNJgx0PkfAwuQwwjVfgiIowECCGzopH32EHsYGDyADt4OdTDMdgYmqMP/3z7G/Q/IPAqVhCuA+w7EvXbgJ+PlfUyfD+zfzzCFw55hQxMryJb/gdV/FoKte1WlyoCsE+g4ZG/NBgggRmwhiiuEYXx4IMCAWNttRlg4bkW2Dh2AFa2uY/JiYmW4j+QzRgxFbBxMDOsbWYxX1yLkIk/KvV71UgKhCAR+//n7v7GhimG5szRDyWc1hv//GP5/8WVFOLzchJnBgPnRe4YD2QyXnWQYTQK+ZJkEMIih+M6A+T7cGmZGxv//sDkci2OZkfkAAQQPTKRIRgHACO8ARng5kmHI4D8sLFmwBJ04EF+E0gzYNOIMbxA4upj7PzM72FsvkAxZiS38cSYBEHjz6CfDunpQKmNgYOVk5nRK+f2DWxho7k+4Wrhhk2/yv8/SYxBg/vkR0yC4aqDyPz/+cuyawvSDgZGZ4cuXrwx7JRQYvrz5w8DKDvfAz2PAPPT3Nx/Df6Z/sw+iGwSyUpf7PUOlzheg9/5zwMSXO3EALQD5+h8DK/d/hhsHeLhVrb4y/PsNjIwfjGwoLoKVDFDAjhawmkB8Hog5fn9lZFA2hRgCTgMswIIBObBxJWxo4roOMgTKrQLiW/DYYmH4h24QA05DGRlykPjtQKwOZYsA8VWQEoAAIip3EwPAYdS33YGBhe0/NmeJmz1geah49xE7vlSNz2sgsAuUOP8zMrIjafyPK4VjM6gaqtAVhwX/cXqNmQ0sbQd0/H50w5n//nsHpIQIhRFY07Ye1uNvnnAcZGJCMeQvEFvJ338sTExggzX++f3v5+EFvxm29rL/+/MXLF4Mde1xHGURjtwP9DUTMyPDrx9/mTY0MR0/MJerD6SNiRkzIqNPK+ge/6nAgD+wQTmfnYn148tfDOubmBk2d7FhaPj37x/npIv/GfwOiDK85lT4hzP3w6s9oAt/fv/DkHFDi+H1i48MbJxw5wHL8/8M3JycDFm7/vw8WIvbIIZfv38zOFhbM4SEhTEIyzMwbLhWwPD6DjPDoYXAAPz/n+nv7/8MqhZcDIYBX5ixuujP378MqVr/ld0EngN5a4BV1RpI9DlJMQjL/mQIrGFgePuEXUlM4SfD3z9fGP7+QEtHQBcwukj8YMjQ/MnA+PubEFKiQ44pflBRIizzU/HvHxwJcrX9O3mmv8Ay9Te28GJ8C6R3AnEURhQyMfxDiTWgIQo40wk7lzA2Q9CzCxOOcgi1EEeAV6DUjrc6wmIgOvCBVlOg1M4Jrfs48KUjVMP+/PwOzbTfkeRA8WUAzUpgABCgF6sJbSKIwm92N8lu07SJSbchpkFNlSJFRVFJsVir0oNYKFLNrSBeFRU8efJQxYN4Es8K4tWDgpegeBAKBQVBBD2klGj/UGs1yc7+jG82SduN7WbTFge+S3Zn98ub977vvXUopC1wAebip/++iVpEPf1Jfxb/Npdx+Teu8a2JYyOBdFutiEsIbi6zxaCSqXsRW0f/3K41DHX92ou4gRhFBJuNQlNetM7iSpRFXEEcgv+0HIR+//D1t3XQC5IMFy2KmcsaZgQv5kepqQK/69qWE8o9NMZR+/r9ssiSvQGr53gJQqohWJQAM5fJTSDuIp5DVXMk3dam65s5qjUJSX7BliisPDL9oUzy73hjJ0DHTiV/NEtHQp36e3OJeHmbI66WGFj45VfD4dK0uJkcwn6OIERYnKHzL++xPGMS11Xo6lUgtd+AgGxC2w69Ibvzb2JDgsBelDW1cxg9cCDdDt0sj0zNjVcZN8raKnwsI4CPPNjN4lHH94AsKJAvFEAUBWiRZcden2TLCwkqMuRwNs3N6lCmHfijUPL7fE9yGyC07AX8uAzsVDRKQY1G4NjJPhgaPQivvtyH0OsICOgRnydKyxFeVRiV/SYDXcPIRhVIH5HESMKUPEeI4MOx54CyRvVopL0c2xaF8Tu3IbE96bhvam4ShIAO+4Y027MOnMHcwzzXsRgWZ/zw9qlV1IumEd+tQM+AAWpaR2IaGBQNX3O6nFRPoDKNGqBpGpzrJmwwHSJddDrDBdm++OAEt0bHQ7izweAKyVpD4POzZCylXR25CWNMgBjT/9jHjCRWQkZccmg4sZRSw63Qp5ogFxcqt9P5RgpM1nBhbpYD9QS9OJ2DUDb5fRcAdtLFpnSEYZNiCgS+Wgy6mtEcrFjTJzPqtf/wNAZXn4w1SNzI8E86hudW32OFeSdYUfDHiMPVPTzBTlXVfdOEPETPbqr4NH0Z0VI15jHE5Kp9fIo/W93HG7VbiJ/1ncOWjbBbtf4CeCN9Q4hB5P8AAAAASUVORK5CYII=';  
        
        if( window.innerWidth < 960 ) {
             $scope.canvasW = window.innerWidth - 50; 
        }
        
        $scope.canvasH = $scope.getCanvasHeight( $scope.canvasW );
        
        $scope.mainCanvas = document.querySelector('#asset-canvas-main');
        $scope.mainContext   = $scope.mainCanvas.getContext('2d');
        
        var image = new Image();
        image.src = $scope.src;
        image.onload = function() {
            var dt = new Date();
            var date_time = dt.toJSON().slice(0,10) + ' ' + dt.toJSON().slice(14,19);
            $scope.mainContext.drawImage(image, 0, 0, $scope.mainCanvas.width, $scope.mainCanvas.height);
            $scope.showWatermark('Date : ' + date_time, 65 );
            $scope.showWatermark('Company : ' + (($scope.asset.company_name==null)?'-':$scope.asset.company_name), 50 );
            $scope.showWatermark('Team : ' + (($scope.asset.team_name==null)?'-':$scope.asset.team_name), 35 );
            $scope.showWatermark('Lng : ' + $scope.asset.longitude, 20 );
            $scope.showWatermark('Lat : ' + $scope.asset.latitude, 5 );
            
            var imageWatermark = new Image();
            imageWatermark.src = $scope.logo;
            imageWatermark.onload = function() {
//                imageWatermark.style.filter       = "alpha(opacity=75)";
//                imageWatermark.style.MozOpacity   = 0.75;
//                imageWatermark.style.opacity      = 0.75;
//                imageWatermark.style.KhtmlOpacity = 0.75;
                $scope.mainContext.drawImage(imageWatermark, $scope.mainCanvas.width - 40 , $scope.mainCanvas.height - 40, 36, 36);
                $scope.canvasToPicture();
            }
        }; 
    }
    
    $scope.showWatermark = function(watermarkText, y) {
        $scope.mainContext.font = "12px Georgia";
        $scope.mainContext.fillStyle = 'black';
        $scope.mainContext.fillText( watermarkText, 5,$scope.canvasH - y);
        $scope.mainContext.strokeStyle = 'white';
        $scope.mainContext.strokeText( watermarkText, 5,$scope.canvasH - y);
    }
    
    $scope.canvasToPicture = function(){
        $ionicPlatform.ready(function() {
            $timeout(function(){
                $scope.src = $scope.mainCanvas.toDataURL("image/jpeg", 1);   
                                
                var param = {
                    server_id : new Date().valueOf(),
                    uid: $scope.uid,
                    asset_id: $scope.asset.server_id,
                    name: $scope.uid + '_' + Date.now() + '.jpg',
                    filesize: 0,
                    res_name: $scope.asset.asset_name,
                    file_type: 'image\/jpeg',
                    base64: $scope.mainCanvas.toDataURL("image/jpeg", 1),
                    action:null
                };
                
                assetImagesModel.add( param, 'C' ).then(function(){
                    $scope.src = '';
                    $scope.getLocalPictures();
                });
                
            }, 800);
        });
    }
    
    $scope.takeAPicture = function(){
        if( $scope.$parent.isMobile() === true ){
            var options = {
              quality: 75,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType: Camera.PictureSourceType.CAMERA,
              allowEdit: true,
              encodingType: Camera.EncodingType.JPEG,
              targetWidth: 960,
              targetHeight: 600,
              saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.newImage = imageData;
                $scope.src = "data:image/jpeg;base64," + imageData;
                $scope.showMainImage();
            }, function(err) {
              // error
            });
        }
    }
    
    $scope.uploadAll = function( ){
        //console.log( $scope.pictures );        
        if( $scope.pictures.length > 0 ){
            
            var q = $q.defer();
            var promises = [];
            
            for( var i=0;i<$scope.pictures.length;i++ ){
                if( $scope.pictures[i].action.toLowerCase() === 'c' ){
                    promises.push( $scope.upload( $scope.pictures[i], i ) );
                }
            }
            
            $q.all(promises).then(function() {
                $timeout(function(){
                    $scope.getLocalPictures();
                }, 2000)
            });
        }
    }
    
    $scope.upload = function( picture, index ){
        if( $scope.$parent.isOnline() === false ){
            $ionicPopup.alert({
                title: amtissData.getAppTitle(),
                template: amtissData.getMessage('no_internet_connection')
            }); 
            return;
        }
        
        //$timeout( function() {
        picture.isajax = 1;
        picture.token = amtissData.getToken();
        picture.index = index;

         amtissService.doRequest( 'assetimages', picture )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    assetImagesModel.updateserverid( response.server_id, response.client_id);
                    $scope.checkShowButton();
                    if( response.index > 0 ){
                        //if( typeof($scope.pictures[index]) !== 'undefined' ){
                           $scope.pictures[index].action = 'O';
                        //}
                            
                        //console.log( $scope.pictures[index]);
                    }
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
    }
    
    $scope.delete = function( id, asset_id){
        //console.log( id + '  ' + asset_id );
        
        if( $scope.$parent.isOnline() === true ){            
            if( $scope.startingOfflineId < asset_id ){
                //delete
                assetImagesModel.removebyid( id );
                
                $timeout(function(){
                    $scope.getLocalPictures();
                }, 1000);
                
            }else{
                //delete on the server
                
                //fetch all picture with action = 'D'
                //then delete it on the server
                //onsuccess delete on local
                assetImagesModel.flagbyid(id).then(function(){
                    assetImagesModel.getflagged().then(function( result ){
                        //console.log( result );
                        var q = $q.defer();
                        var promises = [];
                        
                        angular.forEach(result, function(value, key) { 
                           //promises.push(console.log( value ));
                            
                            //if( value.server_id == 0 ){
                            if( value.server_id > amtissData.getStartingOfflineId() ){
                                promises.push(assetImagesModel.removebyid( id ));
                            }else{
                                promises.push($scope.deleteAtServer( id, value.server_id ));
                            }
                        });
                        
                        $q.all(promises).then(function() {
                            $timeout(function(){
                                $scope.getLocalPictures();
                            }, 1000);
                        });
                        
                    });
                });
            }
            
        }
        else{
            //console.log( $scope.startingOfflineId + ' < ' +asset_id );
            if( $scope.startingOfflineId < asset_id ){
                //delete
                assetImagesModel.remove( id );
            }
            else{
                //update action to 'D'
                assetImagesModel.flagbyid( id );
            }
            
            $timeout(function(){
                $scope.getLocalPictures();
            }, 1000);
        }
    }
    
    $scope.deleteAtServer = function( id, server_id ){
        var picture = {isajax:1, token: amtissData.getToken(), server_id:server_id, action:'D' };
        amtissService.doRequest( 'assetimages', picture )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    assetImagesModel.removebyid( id );
                    $timeout(function(){
                        $scope.getLocalPictures();
                    }, 1000);
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
    }
    
})

.controller('AssetSurveysCtrl', function($scope, $stateParams, $ionicPlatform,
                                            $timeout,
                                            $ionicPopup,
                                            $q,
                                            $ionicHistory,
                                            amtissData,
                                            assetModel,
                                            assetSurveyModel,
                                            amtissService ){
    $scope.uid = 0;
    $scope.asset = {};
    $scope.data = {};
        
    $scope.init = function(){
        $scope.assetId = $stateParams.assetId;
        $scope.uid = amtissData.getUid();
        $scope.startingOfflineId = amtissData.getStartingOfflineId();
        $scope.token = amtissData.getToken();
        assetSurveyModel.getbyassetid( $scope.assetId ).then(
            function(result){
                //console.log( result );
                $scope.data = result;
            }
        );
        
         assetModel.getbyid( Number($scope.assetId), $scope.uid ).then(
            function( result ){
                $scope.asset = result;
                $scope.surveyId = result.survey_id;
//                console.log( result );
//                if( typeof( result.survey_id ) !== 'undefined' ){
//                    if( result.survey_id > 0 ){
//                        if( $scope.$parent.isOnline() === true ){
//                            //set as delete
//                            //assetImagesModel.flagbyassetid( $scope.assetId ).then(function( result ){
//                                //download picture and insert
//                            //if( assetSurveyModel.getReferrer() === 'init') {
//                                $scope.syncSurveys( $scope.token, $scope.assetId );
//                            //}else{
//                                //$scope.getLocalSurvey( result.survey_id );
//                            //
//                            //});
//                            //console.log( result );
//                            //console.log( $ionicHistory.currentHistoryId() );
//                        }else{
//                            //$scope.getLocalSurvey( result.survey_id );
//                        }
//                    }
//                }
            }
        );    
    }
    
//    
//    $scope.syncSurveys = function( token, assetId ){
//        if( $scope.isOnline() === false ){
//            return;
//        }
//        
//        $scope.param = {'isajax':1, 'token':token, 'assetID': assetId };
//        
//        amtissService.doRequest( 'getassetsurveys', $scope.param, true )
//        .then(
//            function(  response ) { 
//                //console.log( response );
//                if( response.status === true ){
//                    //console.log(response.data);
//                     assetSurveyModel.flagbyassetid( assetId ).then(function(){
//                        var inspections = response.data.inspections;
//                        //console.log( inspections );
//                        var q = $q.defer();
//                        var promises = [];
//                         
//                        if( inspections != null ){
//                            for( var i=0;i<inspections.length;i++ ){
//                                //console.log( inspections[i].server_id );
//                                //console.log( inspections[i] );
//                                promises.push( assetSurveyModel.sync( inspections[i] ) );
//                            }
//                        }
//                         
//                         $q.all(promises).then(function() {
//                            $scope.data = inspections; 
//                            //console.log( inspections );
//                            assetSurveyModel.removebyflag();
//                         })
//                    });
//                }
//            }
//        );
//   }
//    $scope.syncSurveyTest = function(){
//        $scope.param = {'isajax':1, 'token':$scope.token, 'assetID': $scope.assetId };
//        amtissService.doRequest( 'getassetsurveys', $scope.param )
//        .then(
//            function(  response ) { 
//                //console.log( response );
//                if( response.status === true ){
//                    //console.log(response.data);
//                     assetSurveyModel.flagbyassetid( $scope.assetId ).then(function(){
//                        var inspections = response.data.inspections;
//                         
//                        var q = $q.defer();
//                        var promises = [];
//                         
//                        if( inspections != null ){
//                            for( var i=0;i<inspections.length;i++ ){
//                                console.log( inspections[i].server_id );
//                                promises.push( assetSurveyModel.sync( inspections[i] ) );
//                            }
//                        }
//                         
//                         $q.all(promises).then(function() {
//                            $scope.data = inspections; 
//                            assetSurveyModel.removebyflag();
//                         })
//                    });
//                }
//            }
//        );
//    }
})

.controller('AssetSurveyCtrl', function($scope, $rootScope, $stateParams, $ionicPlatform,
                                            $timeout,
                                            $ionicPopup,
                                            $q,
                                            $state,
                                            $base64,
                                            $ionicLoading,
                                            LocalStorage, 
                                            amtissData,
                                            assetModel,
                                            assetSurveyModel,
                                            assetSurveyTemplateModel,
                                            workOrderModel,
                                            amtissService){
    $scope.uid = 0;
    $scope.data = {};
    $scope.raw  = {};
    $scope.startingOfflineId = 0;
    
    $scope.init = function(){
        //url: "/assetsurvey/:serverId/:surveyId/:assetId/:assignmentLineId",    
        //url: "/assetsurvey/:userInputId/:surveyId/:assetId/:assignmentLineId",    //

        //console.log( 'action type: ' + $rootScope.actionType );
            $scope.$parent.getPosition().then(function( position ){
                //console.log( position );
                $scope.data.latitude = position.lat;
                $scope.data.longitude = position.lng;
                //console.log( $scope.data );
            });
        
            $scope.data.user_input_id = Number($stateParams.serverId);
            $scope.data.assignment_line_id = Number($stateParams.assignmentLineId);
            $scope.data.uid = amtissData.getUid();

            $scope.token = amtissData.getToken();

            $scope.startingOfflineId = amtissData.getStartingOfflineId();
        
            if( $scope.data.user_input_id == 0 ){//questions from local
//                $scope.data.user_input_id = 0;
                $scope.data.survey_id     = Number( $stateParams.surveyId );
                $scope.data.asset_id      = Number( $stateParams.assetId );
                //console.log( $scope.data.survey_id );
                assetSurveyTemplateModel.getbysurveyid( $scope.data.survey_id ).then(function( result ){   
                    //$scope.raw = result;
                    //console.log( result );

                    var arrayQuestions = [];
                    angular.forEach(result, function(value, key) { 
                        arrayQuestions.push( LocalStorage.get(value.json) );
                    });

                    $scope.test = arrayQuestions;
                    $scope.testPage  = $scope.test;
                    $scope.testQuestion = $scope.test;

                    $scope.data.length = 0;
                    $scope.data.pages = [];
                    //$scope.length = 0;
                    $scope.activePage = 0;



                    var pageName = '';

                    angular.forEach($scope.testPage, function(value, key) { 
                        if( value.page_title !== pageName ){
                            var page = {};
                            page.title = value.page_title;
                            page.id    = value.page_id;

                            var questions = [];
                            angular.forEach($scope.testQuestion, function(value, key) { 
                                if( page.title == value.page_title){
                                    questions.push( value );
                                }
                            });

                            page.questions = questions;

                            $scope.data.pages.push(page);
                            $scope.data.length += 1;
                            pageName = page.title;
                        }
                    });
                });
            }
//            else if( $scope.data.server_id > $scope.startingOfflineId ){
//                assetSurveyModel.getbyserverid( $scope.data.server_id ).then(function(result){
//                    //console.log( result );
//                    //$scope.raw = result.json;
//                    $scope.test = LocalStorage.get( result.json );
//                    $scope.testPage  = $scope.test;
//                    $scope.testQuestion = $scope.test;
//
//                    $scope.data.length = 0;
//                    $scope.data.pages = [];
//                    //$scope.length = 0;
//                    $scope.activePage = 0;
//                    
//                    $scope.data.inspection_id       = result.server_id;
//                    $scope.data.assignment_line_id  = result. assignment_line_id;
//                    $scope.data.asset_id            = result.asset_id;
//                    $scope.data.user_input_id       = result.user_input_id;
//                    $scope.data.survey_id           = result.survey_id;
//
//                    var pageName = '';
//
//                    angular.forEach($scope.testPage, function(value, key) { 
//                        if( value.page_title !== pageName ){
//                            var page = {};
//                            page.title = value.page_title;
//                            page.id    = value.page_id;
//
//                            var questions = [];
//                            angular.forEach($scope.testQuestion, function(value, key) { 
//                                if( page.title == value.page_title){
//                                    if( value.type == 'datetime' ){
//                                        //console.log( value.type);
//                                        if( typeof(value.survey_answer) != 'undefined' ){
//                                            value.survey_answer = new Date( value.survey_answer );
//                                        }
//                                    }
//                                    questions.push( value );
//                                    //console.log(value);
//                                }
//                            });
//
//                            page.questions = questions;
//
//                            $scope.data.pages.push(page);
//                            $scope.data.length += 1;
//                            pageName = page.title;
//                        }
//                    });
//                });
//        
//            }
            else{
                assetSurveyModel.getbyuserinputid( $scope.data.user_input_id ).then(function(result){
                    //console.log( result );
                    //$scope.raw = result.json;
                    $scope.test = LocalStorage.get( result.json );
                    $scope.testPage  = $scope.test;
                    $scope.testQuestion = $scope.test;

                    $scope.data.length = 0;
                    $scope.data.pages = [];
                    //$scope.length = 0;
                    $scope.activePage = 0;
                    
                    $scope.data.inspection_id       = result.server_id;
                    $scope.data.assignment_line_id  = result.assignment_line_id;
                    $scope.data.asset_id            = result.asset_id;
                    $scope.data.user_input_id       = result.user_input_id;
                    $scope.data.survey_id           = result.survey_id;

                    var pageName = '';

                    angular.forEach($scope.testPage, function(value, key) { 
                        if( value.page_title !== pageName ){
                            var page = {};
                            page.title = value.page_title;
                            page.id    = value.page_id;

                            var questions = [];
                            angular.forEach($scope.testQuestion, function(value, key) { 
                                if( page.title == value.page_title){
                                    if( value.type == 'datetime' ){
                                        //console.log( value.type);
                                        if( typeof(value.survey_answer) != 'undefined' ){
                                            value.survey_answer = new Date( value.survey_answer );
                                        }
                                    }
                                    questions.push( value );
                                    //console.log(value);
                                }
                            });

                            page.questions = questions;

                            $scope.data.pages.push(page);
                            $scope.data.length += 1;
                            pageName = page.title;
                        }
                    });
                });
            }
        
        //console.log( $scope.data );
    }
    
    $scope.next = function( index ){
        $scope.activePage = index + 1;
        $scope.data.last_displayed_page = $scope.activePage + 1;
    }
    
    $scope.syncByUserInputId = function( assetId, surveyUserInputId) {
        
        $scope.param = {'isajax':1, 'token':$scope.token, 'assetID': assetId , 'surveyUserInputID':surveyUserInputId};
        amtissService.doRequest( 'getassetsurvey', $scope.param )
        .then(
            function(  response ) { 
                console.log( response );
                if( response.status === true ){
                    //console.log(response.data);
                    var inspections = response.data.inspections;
                    if( inspections != null ){
                        for( var i=0;i<inspections.length;i++ ){
                            console.log( inspections[i].server_id );
                            assetSurveyModel.sync( inspections[i] );
                        }
                    }
                }
            }
        );  
    }
    
    
    $scope.surveySave = function( valid ){
        if( valid ){
            if( $scope.$parent.isOnline() !== true ){
                $ionicPopup.alert({
                    title: amtissData.getAppTitle(),
                    template: amtissData.getMessage('offline_mode')
                }); 
                
//                console.log( $scope.data.server_id );
//                console.log( typeof($scope.data.server_id) );
   
                if( Number($rootScope.actionType) === 0){ //ad hock
                    $timeout(function(){
                        $ionicLoading.show({});
                    }, 200);
                     
                    if( $scope.data.server_id === 0 ){
                        $scope.data.server_id = new Date().valueOf();
                    }
//                    
                    if( typeof( $scope.data.date_create ) === 'undefined' ){
                        var dt      = new Date();
                        var month   = dt.getMonth() + 1;
                            month   = month < 10? ('0'+month):month;
                        var day     = dt.getDay();
                            day     = day < 10? ('0'+day):day;
                        $scope.data.date_create = dt.getFullYear()+'-'+ month +'-'+ day;
                    }
//                    console.log( typeof($scope.data.last_displayed_page) );
//                    console.log( $scope.data.pages.length );
                    if( typeof($scope.data.last_displayed_page) === 'undefined' || $scope.data.last_displayed_page === null ){
                        $scope.data.last_displayed = $scope.data.pages.length;
                    }else{
                        $scope.data.last_displayed = $scope.data.last_displayed_page;
                    }
                    
                    $scope.data.state               = 'done';
                    $scope.data.assignment_line_id  = null;
                    
                    var arrQuestions = [];
                    if( typeof($scope.data.pages) !== 'undefined' ){
                        for( var i=0;i<$scope.data.pages.length; i++ ){
                            if( typeof($scope.data.pages[i].questions) !== 'undefined' ){
                                for(var j=0;j<$scope.data.pages[i].questions.length;j++){
                                    arrQuestions.push($scope.data.pages[i].questions[j]);
                                }
                            }
                        }
                    }
                    
                    $scope.data.surveys             = arrQuestions;
                    console.log( $scope.data );
                    assetSurveyModel.syncOffline( $scope.data );
                    
                    $timeout(function(){
                        $ionicLoading.hide();
                        $state.go('app.assetsurveys', {'assetId':$scope.data.asset_id});
                    }, 1500);
                }
                else{
                    
                    //console.log( $scope.data );return;
                    $timeout(function(){
                        $ionicLoading.show({});
                    }, 200);
                     
                    if( $scope.data.server_id === 0 ){
                        $scope.data.server_id = new Date().valueOf();
                    }
                    if( $scope.data.user_input_id === 0 ){
                        $scope.data.user_input_id = new Date().valueOf();
                    }
                    
                    if( typeof( $scope.data.date_create ) === 'undefined' ){
                        var dt      = new Date();
                        var month   = dt.getMonth() + 1;
                            month   = month < 10? ('0'+month):month;
                        var day     = dt.getDay();
                            day     = day < 10? ('0'+day):day;
                        $scope.data.date_create = dt.getFullYear()+'-'+ month +'-'+ day;
                    }
//                    console.log( typeof($scope.data.last_displayed_page) );
//                    console.log( $scope.data.pages.length );
                    if( typeof($scope.data.last_displayed_page) === 'undefined' || $scope.data.last_displayed_page === null ){
                        $scope.data.last_displayed = $scope.data.pages.length;
                    }else{
                        $scope.data.last_displayed = $scope.data.last_displayed_page;
                    }
                    
                    $scope.data.state               = 'done';
                    //$scope.data.assignment_line_id  = null;
                    
                    var arrQuestions = [];
                    if( typeof($scope.data.pages) !== 'undefined' ){
                        for( var i=0;i<$scope.data.pages.length; i++ ){
                            if( typeof($scope.data.pages[i].questions) !== 'undefined' ){
                                for(var j=0;j<$scope.data.pages[i].questions.length;j++){
                                    arrQuestions.push($scope.data.pages[i].questions[j]);
                                }
                            }
                        }
                    }
                    
                    $scope.data.surveys             = arrQuestions;
                    
                    assetSurveyModel.syncOffline( $scope.data );
                    
                    $timeout(function(){
                        $ionicLoading.hide();
                        $state.go('app.workordersassets', {'actionType':$rootScope.actionType});
                    }, 1500);
                }
            }
            else{
                if( $rootScope.actionType == 0){ //ad hock
                    
                    if( typeof($scope.data.last_displayed_page) === 'undefined' || $scope.data.last_displayed_page === null ){
                        $scope.data.last_displayed_page = $scope.data.length;
                    }
                    
                    var client_id = $scope.data.user_input_id;
                    var isSyncOffline = false;
                    $scope.data.server_id = 0;
//                    console.log( Number($scope.data.user_input_id) +" > " + Number($scope.startingOfflineId) );
//                    console.log( $scope.data.inspection_id );
//                    console.log( typeof($scope.data.inspection_id) );
                    if( Number($scope.data.user_input_id) > Number($scope.startingOfflineId) ){
                        $scope.data.server_id = 0;
                        isSyncOffline = true;
                    }else{
                        $scope.data.server_id = typeof($scope.data.inspection_id) == 'undefined'?0:$scope.data.inspection_id;
                    }
                    
                    $scope.param = {'isajax':1, 'token':$scope.token, 'client_id':client_id,
                                    'data': LocalStorage.set( $scope.data ) };
                    console.log( $scope.param );
                    //return;
                    $ionicLoading.show({});
                    amtissService.doRequest( 'updatesurvey', $scope.param )
                    .then(
                        function(  response ) { 
                            console.log( response );
                            //TODO: UPDATE LOCAL
                            $ionicLoading.hide();
                            if( response.status === true ){
                                
                                //update server_id, user_input_id, action
                                if( isSyncOffline ){
                                    console.log('test');
                                    assetSurveyModel.updateserverid(response.inspection_id, 
                                                                    response.user_input_id, response.client_id);
                                }
                                
                                $timeout(function(){
                                    $scope.syncByUserInputId( $scope.data.asset_id, response.user_input_id );
                                }, 500);
                                
                                $timeout(function(){
                                    $state.go('app.assetsurveys', {'assetId':$scope.data.asset_id});
                                }, 2000);
                            }
                        }
                    )
                }else if( $rootScope.actionType > 0 ){ //workorder
                    /* amtiss_assignment */ 
                    // amtiss_assignment id untuk update state -> didapat dari $rootScope.actionType
                    //state --> cek di server (assigned, progress, submitted
                    /* amtiss_asset_inspection */
                    //latitude --> didapat dari check geo $scope.data.latitute
                    //longitude  --> didapat dari check geo
                    //assignment_line_id
                    $scope.param = {'isajax':1, 'token':$scope.token, 
                                    'assignment_id':$rootScope.actionType,
                                    //'assignment_line_id':$rootScope.assignment_line_id,
                                    'data': LocalStorage.set( $scope.data ) };
                    //console.log( $scope.param );
                    //return;
                    $ionicLoading.show({});
                    amtissService.doRequest( 'updateworkorder', $scope.param )
                    .then(
                        function(  response ) { 
                            console.log( response );
                            $ionicLoading.hide();
                            if( response.status === true ){

                                $scope.syncByUserInputId( response.assetid, response.newid );
                                $scope.updateWorkorder( $rootScope.actionType, $scope.data.assignment_line_id, response.newid, response.wo_state );
                                $timeout(function(){
                                    $state.go('app.workordersassets', {'actionType':$rootScope.actionType});
                                }, 1000);
                            }
                        }
                    )
                }
            }
        }
    }
    
    $scope.updateWorkorder = function(wo_id, assignment_line_id, user_input_id, state){
//        console.log( wo_id );
//        console.log( assignment_line_id );
//        console.log( user_input_id );
        workOrderModel.getbyid( wo_id ).then(function( data ){
            var json = LocalStorage.get(data.json);
            if( json.length > 0 ){
                for( var i=0;i<json.length;i++){
                    if( json[i].id == assignment_line_id){
                        json[i].user_input_id = user_input_id;
                    }
                }
                workOrderModel.updatejsonbyid( state, LocalStorage.set(json), wo_id);
            }
        });
    }
})


.controller('WorkordersCtrl', function($scope, $ionicPlatform, $timeout, $ionicLoading,
    $ionicPopup, $cordovaSQLite, amtissData, $ionicPopover) {
        
    var workorderAndType        = [];
    var workorders              = [];
    $scope.workorderterm        = '';
    $scope.data                 = [];
    
    //Context menu
    $ionicPopover.fromTemplateUrl('templates/assetFilter.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
        popover.hide();
    });
    
    $scope.init = function(){
        $ionicPlatform.ready(function() {
            $timeout(function(){
                /* TODO: OPEN LATER remove unwanted asset */
                //assetModel.removebyflag ( amtissData.getUid() );

                $scope.showList( 'all', '' );//type, term

                 $ionicLoading.show({
                    //template: 'Loading...'
                });
            }, 200);
            
            
            $timeout( function(){
                $ionicLoading.hide();
            },300);
        });
    }
    
    $scope.getListByFilter = function( type, term, name){
        
        var workordersByFilter = [];
        var uid = amtissData.getUid();
        //if( type === 'category' ){
            var qq = "SELECT * FROM workorders WHERE ( uid = ? ) AND ( " + type + "_name = ? ) ORDER BY server_id DESC";//, " + type + "_name ";
            var where = [uid, name];
                            
            if( term !== ''){
                qq = "SELECT * FROM workorders WHERE ( uid = ? ) AND ( " + type + "_name = ? ) " 
                    + " AND ( ( name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) ) "
                    + " ORDER BY server_id DESC ";// + type + "_name ";
                where = [uid, name, '%' + term + '%', '%' + term + '%', '%' + term + '%' , '%' + term + '%' ];
            } 
        
            workordersByFilter.length = 0;

            $cordovaSQLite.execute(db, qq, where).then(function(subresult) {
                if(subresult.rows.length > 0) {
                    
                    for(var i=0;i<subresult.rows.length;i++){
                        //var asset = subresult.rows.item(i);
                        
                        var asset = subresult.rows.item(i);
                        workordersByFilter.push( asset );
                    }
                    
                } else {
                    console.log("NO ROWS EXIST");
                }
            }, function(error) {
                console.error(error.message);
            });
        
        return workordersByFilter;
    };
        
    $scope.showList = function( type, term ){
        $scope.data.length      = 0;
        $scope.type = type;
        
        $scope.workorderterm = term;
        
        var uid = amtissData.getUid();

        if( type === 'all'){    
            var q = "SELECT *" +
                    " FROM workorders "+
                    " WHERE uid = ? " +
                    " GROUP BY server_id ORDER BY server_id DESC";
            var where = [uid];
                
            if( term !== '' ){
                q       = " SELECT * FROM workorders WHERE (uid = ?)  AND "
                        + " ( ( name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) )  ORDER BY server_id DESC";
                where   = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%'];
            }
            
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                workorders.length = 0;
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length;i++){
                        var workorder = result.rows.item(i);
                        workorders.push( workorder );
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: amtissData.getMessage( 'no_wo_found' )
                         });
                    return;
                }
                    
                $scope.data = [{'type':'All', 'workorders': workorders}];
                console.log( $scope.data[0].workorders );
            });
        }else if( type === 'category' ){
            //console.log (' Type Category ');
            workorderAndType.length  = 0;
            
            //get available category sort by name
            var q = "SELECT DISTINCT category_name FROM workorders WHERE ( uid = ? ) AND ( category_name IS NOT NULL ) ORDER BY category_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT category_name FROM workorders WHERE ( uid = ? ) AND ( category_name IS NOT NULL ) " 
                    + " AND ( ( name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) ) "
                    + " ORDER BY category_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
            
                            
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).category_name;           
                        workorderAndType.push({'type':name, 'workorders':$scope.getListByFilter( type, term, name) });
                            
                        $scope.data = workorderAndType;
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_wo_found' )
                    });
                    return;
                }
            }, function(error) {
                console.error(error);
            });
        }else if( type === 'location' ){
            //console.log (' Type Location ');
            workorderAndType.length  = 0;
            
            //get available location sort by name
            var q = "SELECT DISTINCT location_name FROM workorders WHERE ( uid = ? ) AND ( location_name IS NOT NULL ) ORDER BY location_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT location_name FROM workorders WHERE ( uid = ? ) AND ( location_name IS NOT NULL ) " 
                    + " AND ( ( name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) ) "
                    + " ORDER BY location_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
            
                            
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).location_name;           
                        workorderAndType.push({'type':name, 'workorders':$scope.getListByFilter( type, term, name) });
                            
                        $scope.data = workorderAndType;
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_wo_found' )
                    });
                    return;
                }
            }, function(error) {
                console.error(error);
            });
        }else if( type === 'team' ){
            //console.log (' Type Team ');
            workorderAndType.length  = 0;
            
            //get available team sort by name
            var q = "SELECT DISTINCT team_name FROM workorders WHERE ( uid = ? ) AND ( team_name IS NOT NULL ) ORDER BY team_name ";
            var where = [uid];
                
            if( term !== ''){
                q = "SELECT DISTINCT team_name FROM workorders WHERE ( uid = ? ) AND ( team_name IS NOT NULL ) " 
                    + " AND ( ( name LIKE ? ) OR ( category_name LIKE ? ) OR ( location_name LIKE ? ) OR ( team_name LIKE ? ) ) "
                    + " ORDER BY team_name ";
                where = [ uid, '%' + term + '%', '%' + term + '%', '%' + term + '%', '%' + term + '%' ];
            }
            
                            
            $cordovaSQLite.execute(db, q, where).then(function(result) {
                if(result.rows.length > 0) {
                    for(var i=0;i<result.rows.length; i++){
                        var name = result.rows.item(i).team_name;           
                        workorderAndType.push({'type':name, 'workorders':$scope.getListByFilter( type, term, name) });
                            
                        $scope.data = workorderAndType;
                    }
                } else {
                    var alertPopUp = $ionicPopup.alert({
                          title: amtissData.getAppTitle(),
                          template: amtissData.getMessage( 'no_wo_found' )
                    });
                    return;
                }
            }, function(error) {
                console.error(error);
            });
        }
    }
})

.controller('WorkorderImagesCtrl', function($scope, $rootScope, $stateParams, $ionicPlatform,
                                            $timeout,
                                            $ionicPopup,
                                            $cordovaCamera, $q,
                                            amtissData,
                                            assetModel,
                                            workOrderModel,
                                            workorderImagesModel,
                                            LocalStorage,
                                            amtissService){
    $scope.uid = 0;
    $scope.woLine = {};
    $scope.asset  = {};
    $scope.src = '';
    $scope.newImage = {};
    $scope.mainImage
    $scope.mainContenxt;
    $scope.mainCanvas;
    $scope.canvasW = 960;
    $scope.canvasH = 600; 
    $scope.logo;
    $scope.asset.showUploadButton = false;
    
    $scope.pictures = [];
    
    $scope.init = function(){
        $scope.inspectionId = $stateParams.inspectionId;
        $scope.woLineId     = $stateParams.woLineId;
        $scope.assetId      = $stateParams.assetId;
        $scope.uid          = amtissData.getUid();
        $scope.startingOfflineId = amtissData.getStartingOfflineId();
        
        $scope.token = amtissData.getToken();        
        //todo: remove later
        //$scope.src = printer_02;
        $rootScope.actionType = 5;
        
        assetModel.getbyid( Number($scope.assetId), $scope.uid ).then(
            function( result ){
                $scope.asset = result;
                //console.log( result );
                workOrderModel.getbyid( Number($rootScope.actionType) ).then(
                    function( res ){
                        var json = LocalStorage.get(res.json);
                        if( typeof(json) !== 'undefined' ){
                            for( var l=0;l<json.length;l++ ){
                                if(Number($scope.assetId) == Number(json[l].asset_id) ){
                                    $scope.woLine = json[l];
                                    //$scope.showMainImage();
                                    $scope.getLocalPictures();
                                    break;
                                }
                            }
                        }  
                    }
                );
            }
        );
    }
    
    $scope.getLocalPictures = function(){
        $scope.pictures.length = 0;
        workorderImagesModel.getbywolineid( $scope.woLine.id ).then(
            function( result ){
                console.log( result );
                //console.log( result['action'] );
                $scope.pictures = result;
                $scope.checkShowButton();
            }
        );   
    }
    
    $scope.checkShowButton = function(){
        $scope.asset.showUploadButton = false;
        $timeout(function(){
            
            if( $scope.pictures.length > 0 ){
                for( var i=0;i<$scope.pictures.length; i++ ){
                    console.log( $scope.pictures[i].action );
                    if( $scope.pictures[i].action == 'C'){
                        $scope.asset.showUploadButton = true;
                    }
                }
            }
        }, 1000);     
    }
    
    $scope.getCanvasHeight = function( w ){
        return (0.625 * w)
    }
    
    $scope.showMainImage = function(){
        
        $scope.logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAAGWB6gOAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjUxMDY0MTFFNEE5QzExRTVCRTlCQzYxMEQ1QTc4NjM3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjUxMDY0MTFGNEE5QzExRTVCRTlCQzYxMEQ1QTc4NjM3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTEwNjQxMUM0QTlDMTFFNUJFOUJDNjEwRDVBNzg2MzciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTEwNjQxMUQ0QTlDMTFFNUJFOUJDNjEwRDVBNzg2MzciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5GFsoyAAAHPUlEQVR42mL8//8/AzpgARGT9tr///2PkSF2/1OE4MY2FgZ5I3ZGBlYGhr9svJ/BgsvNH4FVrHCW+v/7B+NCJqgxjEAMMpyRleP/H4AAYkS3iGXyQXuG/38Y/kfsfQYR2NDEAlYeYc7AEH5c5j/Ltx8/GDbav/oPNJgx0PkfAwuQwwjVfgiIowECCGzopH32EHsYGDyADt4OdTDMdgYmqMP/3z7G/Q/IPAqVhCuA+w7EvXbgJ+PlfUyfD+zfzzCFw55hQxMryJb/gdV/FoKte1WlyoCsE+g4ZG/NBgggRmwhiiuEYXx4IMCAWNttRlg4bkW2Dh2AFa2uY/JiYmW4j+QzRgxFbBxMDOsbWYxX1yLkIk/KvV71UgKhCAR+//n7v7GhimG5szRDyWc1hv//GP5/8WVFOLzchJnBgPnRe4YD2QyXnWQYTQK+ZJkEMIih+M6A+T7cGmZGxv//sDkci2OZkfkAAQQPTKRIRgHACO8ARng5kmHI4D8sLFmwBJ04EF+E0gzYNOIMbxA4upj7PzM72FsvkAxZiS38cSYBEHjz6CfDunpQKmNgYOVk5nRK+f2DWxho7k+4Wrhhk2/yv8/SYxBg/vkR0yC4aqDyPz/+cuyawvSDgZGZ4cuXrwx7JRQYvrz5w8DKDvfAz2PAPPT3Nx/Df6Z/sw+iGwSyUpf7PUOlzheg9/5zwMSXO3EALQD5+h8DK/d/hhsHeLhVrb4y/PsNjIwfjGwoLoKVDFDAjhawmkB8Hog5fn9lZFA2hRgCTgMswIIBObBxJWxo4roOMgTKrQLiW/DYYmH4h24QA05DGRlykPjtQKwOZYsA8VWQEoAAIip3EwPAYdS33YGBhe0/NmeJmz1geah49xE7vlSNz2sgsAuUOP8zMrIjafyPK4VjM6gaqtAVhwX/cXqNmQ0sbQd0/H50w5n//nsHpIQIhRFY07Ye1uNvnnAcZGJCMeQvEFvJ338sTExggzX++f3v5+EFvxm29rL/+/MXLF4Mde1xHGURjtwP9DUTMyPDrx9/mTY0MR0/MJerD6SNiRkzIqNPK+ge/6nAgD+wQTmfnYn148tfDOubmBk2d7FhaPj37x/npIv/GfwOiDK85lT4hzP3w6s9oAt/fv/DkHFDi+H1i48MbJxw5wHL8/8M3JycDFm7/vw8WIvbIIZfv38zOFhbM4SEhTEIyzMwbLhWwPD6DjPDoYXAAPz/n+nv7/8MqhZcDIYBX5ixuujP378MqVr/ld0EngN5a4BV1RpI9DlJMQjL/mQIrGFgePuEXUlM4SfD3z9fGP7+QEtHQBcwukj8YMjQ/MnA+PubEFKiQ44pflBRIizzU/HvHxwJcrX9O3mmv8Ay9Te28GJ8C6R3AnEURhQyMfxDiTWgIQo40wk7lzA2Q9CzCxOOcgi1EEeAV6DUjrc6wmIgOvCBVlOg1M4Jrfs48KUjVMP+/PwOzbTfkeRA8WUAzUpgABCgF6sJbSKIwm92N8lu07SJSbchpkFNlSJFRVFJsVir0oNYKFLNrSBeFRU8efJQxYN4Es8K4tWDgpegeBAKBQVBBD2klGj/UGs1yc7+jG82SduN7WbTFge+S3Zn98ub977vvXUopC1wAebip/++iVpEPf1Jfxb/Npdx+Teu8a2JYyOBdFutiEsIbi6zxaCSqXsRW0f/3K41DHX92ou4gRhFBJuNQlNetM7iSpRFXEEcgv+0HIR+//D1t3XQC5IMFy2KmcsaZgQv5kepqQK/69qWE8o9NMZR+/r9ssiSvQGr53gJQqohWJQAM5fJTSDuIp5DVXMk3dam65s5qjUJSX7BliisPDL9oUzy73hjJ0DHTiV/NEtHQp36e3OJeHmbI66WGFj45VfD4dK0uJkcwn6OIERYnKHzL++xPGMS11Xo6lUgtd+AgGxC2w69Ibvzb2JDgsBelDW1cxg9cCDdDt0sj0zNjVcZN8raKnwsI4CPPNjN4lHH94AsKJAvFEAUBWiRZcden2TLCwkqMuRwNs3N6lCmHfijUPL7fE9yGyC07AX8uAzsVDRKQY1G4NjJPhgaPQivvtyH0OsICOgRnydKyxFeVRiV/SYDXcPIRhVIH5HESMKUPEeI4MOx54CyRvVopL0c2xaF8Tu3IbE96bhvam4ShIAO+4Y027MOnMHcwzzXsRgWZ/zw9qlV1IumEd+tQM+AAWpaR2IaGBQNX3O6nFRPoDKNGqBpGpzrJmwwHSJddDrDBdm++OAEt0bHQ7izweAKyVpD4POzZCylXR25CWNMgBjT/9jHjCRWQkZccmg4sZRSw63Qp5ogFxcqt9P5RgpM1nBhbpYD9QS9OJ2DUDb5fRcAdtLFpnSEYZNiCgS+Wgy6mtEcrFjTJzPqtf/wNAZXn4w1SNzI8E86hudW32OFeSdYUfDHiMPVPTzBTlXVfdOEPETPbqr4NH0Z0VI15jHE5Kp9fIo/W93HG7VbiJ/1ncOWjbBbtf4CeCN9Q4hB5P8AAAAASUVORK5CYII=';  
        
        if( window.innerWidth < 960 ) {
             $scope.canvasW = window.innerWidth - 50; 
        }
        
        $scope.canvasH = $scope.getCanvasHeight( $scope.canvasW );
        
        $scope.mainCanvas = document.querySelector('#asset-canvas-main');
        $scope.mainContext   = $scope.mainCanvas.getContext('2d');
        
        var image = new Image();
        image.src = $scope.src;
        image.onload = function() {
            var dt = new Date();
            var date_time = dt.toJSON().slice(0,10) + ' ' + dt.toJSON().slice(14,19);
            $scope.mainContext.drawImage(image, 0, 0, $scope.mainCanvas.width, $scope.mainCanvas.height);
            $scope.showWatermark('Date : ' + date_time, 65 );
            $scope.showWatermark('Company : ' + (($scope.asset.company_name==null)?'-':$scope.asset.company_name), 50 );
            $scope.showWatermark('Team : ' + (($scope.asset.team_name==null)?'-':$scope.asset.team_name), 35 );
            $scope.showWatermark('Lng : ' + $scope.asset.longitude, 20 );
            $scope.showWatermark('Lat : ' + $scope.asset.latitude, 5 );
            
            var imageWatermark = new Image();
            imageWatermark.src = $scope.logo;
            imageWatermark.onload = function() {
//                imageWatermark.style.filter       = "alpha(opacity=75)";
//                imageWatermark.style.MozOpacity   = 0.75;
//                imageWatermark.style.opacity      = 0.75;
//                imageWatermark.style.KhtmlOpacity = 0.75;
                $scope.mainContext.drawImage(imageWatermark, $scope.mainCanvas.width - 40 , $scope.mainCanvas.height - 40, 36, 36);
                $scope.canvasToPicture();
            }
        }; 
    }
    
    $scope.showWatermark = function(watermarkText, y) {
        $scope.mainContext.font = "12px Georgia";
        $scope.mainContext.fillStyle = 'black';
        $scope.mainContext.fillText( watermarkText, 5,$scope.canvasH - y);
        $scope.mainContext.strokeStyle = 'white';
        $scope.mainContext.strokeText( watermarkText, 5,$scope.canvasH - y);
    }
    
    $scope.canvasToPicture = function(){
        $ionicPlatform.ready(function() {
            $timeout(function(){
                $scope.src = $scope.mainCanvas.toDataURL("image/jpeg", 1);   
                                
                var param = {
                    server_id : new Date().valueOf(),
                    uid: $scope.uid,
                    wo_id: $rootScope.actionType,
                    wo_line_id: $scope.woLine.id,
                    asset_id: $scope.asset.server_id,
                    name: $scope.uid + '_' + Date.now() + '.jpg',
                    filesize: 0,
                    res_name: $scope.asset.asset_name,
                    file_type: 'image\/jpeg',
                    base64: $scope.mainCanvas.toDataURL("image/jpeg", 1),
                    action:null
                };
                
                workorderImagesModel.add( param, 'C' ).then(function(){
                    $scope.src = '';
                    $scope.getLocalPictures();
                });
                
            }, 800);
        });
    }
    
    $scope.takeAPicture = function(){
        if( $scope.$parent.isMobile() === true ){
            var options = {
              quality: 75,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType: Camera.PictureSourceType.CAMERA,
              allowEdit: true,
              encodingType: Camera.EncodingType.JPEG,
              targetWidth: 960,
              targetHeight: 600,
              saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.newImage = imageData;
                $scope.src = "data:image/jpeg;base64," + imageData;
                $scope.showMainImage();
            }, function(err) {
              // error
            });
        }
    }
    
    $scope.uploadAll = function( ){
        
        if( $scope.pictures.length > 0 ){
            
            var q = $q.defer();
            var promises = [];
            
            for( var i=0;i<$scope.pictures.length;i++ ){
                if( $scope.pictures[i].action.toLowerCase() === 'c' ){
                    promises.push( $scope.upload( $scope.pictures[i] ) );
                    //console.log( $scope.pictures[i] );
                }
            }
            
            $q.all(promises).then(function() {
                $timeout(function(){
                    $scope.getLocalPictures();
                }, 2000)
            });
        }
    }
    
    $scope.upload = function( picture ){
        if( $scope.$parent.isOnline() === false ){
            $ionicPopup.alert({
                title: amtissData.getAppTitle(),
                template: amtissData.getMessage('no_internet_connection')
            }); 
            return;
        }
        
        //$timeout( function() {
        picture.isajax = 1;
        picture.token = amtissData.getToken();
        picture.inspection_id = Number($scope.inspectionId);
        console.log( picture );
        //return;

        amtissService.doRequest( 'workorderimages', picture )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    workorderImagesModel.updateserverid( response.server_id, response.client_id);
                    
                    $scope.checkShowButton();
                    
//                    if( response.index > 0 ){
//                        $scope.pictures[index].action = 'O';
//                    }
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
        //}, 1000);
    }
    
    $scope.delete = function( server_id ){
//        console.log( server_id );
//        console.log( $scope.startingOfflineId );return;
        if( server_id > $scope.startingOfflineId ){
            workorderImagesModel.removebyserverid( server_id );
                
            $timeout(function(){
                $scope.getLocalPictures();
            }, 1000);
        }else{
            if( $scope.$parent.isOnline() === true ){ 
                $scope.deleteAtServer(server_id)
            }else{
                workorderImagesModel.flagbyserverid( server_id );
            }
        }
        
//        if( $scope.$parent.isOnline() === true ){            
//            if( $scope.startingOfflineId < asset_id ){
//                //delete
//                workorderImagesModel.removebyid( id );
//                
//                $timeout(function(){
//                    $scope.getLocalPictures();
//                }, 1000);
//                
//            }else{
//                //delete on the server
//                
//                //fetch all picture with action = 'D'
//                //then delete it on the server
//                //onsuccess delete on local
//                workorderImagesModel.flagbyid(id).then(function(){
//                    workorderImagesModel.getflagged().then(function( result ){
//                        //console.log( result );
//                        var q = $q.defer();
//                        var promises = [];
//                        
//                        angular.forEach(result, function(value, key) { 
//                           //promises.push(console.log( value ));
//                            
//                            if( value.server_id == 0 ){
//                                promises.push(workorderImagesModel.removebyid( id ));
//                            }else{
//                                promises.push($scope.deleteAtServer( id, value.server_id ));
//                            }
//                        });
//                        
//                        $q.all(promises).then(function() {
//                            $timeout(function(){
//                                $scope.getLocalPictures();
//                            }, 1000);
//                        });
//                        
//                    });
//                });
//            }
//            
//        }
//        else{
//            if( $scope.startingOfflineId < asset_id ){
//                //delete
//                assetImagesModel.remove( id );
//            }
//            else{
//                //update action to 'D'
//                assetImagesModel.flagbyid( id );
//            }
//            
//            $timeout(function(){
//                $scope.getLocalPictures();
//            }, 1000);
//        }
    }
    
    $scope.deleteAtServer = function( server_id ){
    
        var picture = {isajax:1, token: amtissData.getToken(), server_id:server_id, action:'D' };
        amtissService.doRequest( 'assetimages', picture )
        .then(
            function( response ) {
                if( response.status === true ){
                    //update server_id dan action
                    workorderImagesModel.removebyserverid( server_id );
                    $timeout(function(){
                        $scope.getLocalPictures();
                    }, 1000);
                }else{
                    $ionicPopup.alert({
                        title: amtissData.getAppTitle(),
                        template: response.message
                    }); 
                }
            }
        );
    }
    
})

.controller('MessagingCtrl', function($scope) {
})

.controller('GuidelineCtrl', function($scope) {
})

.controller('SettingsCtrl', function($scope, $state, $cordovaSQLite,
                                    assetModel, 
                                    userModel,
                                    attributeModel, 
                                    locationModel,
                                    categoryModel,
                                    companyModel,
                                    vendorModel,
                                    teamModel,
                                    attModel ) {
    
    $scope.clearDatabase = function(){
        var q = "DROP TABLE user";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE assets";
            $cordovaSQLite.execute(db, q);
            
            q = "DROP TABLE asset_images";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE attributes";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE locations";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE categories";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE companies";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE vendors";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE teams";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE atts";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE asset_images";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE user_input";
            $cordovaSQLite.execute(db, q);
        
            q = "DROP TABLE survey";
            $cordovaSQLite.execute(db, q);
            
            q = "DROP TABLE survey_template";
            $cordovaSQLite.execute(db, q);
                
            q = "DROP TABLE workorders";
            $cordovaSQLite.execute(db, q);
            
            q = "DROP TABLE workorder_images";
            $cordovaSQLite.execute(db, q);
        
            $state.go('login');
    }
})


.controller('UserCtrl', function($scope) {
});
